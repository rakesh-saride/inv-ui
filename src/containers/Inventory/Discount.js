import "date-fns";
import React, { Component, Fragment } from "react";
import FormControl from "@material-ui/core/FormControl";
import {
  Paper,
  FormHelperText,
  TextField,
  Button,
  Typography
} from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider, DateTimePicker } from "material-ui-pickers";
import DateRange from "@material-ui/icons/DateRange";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import AccessTime from "@material-ui/icons/AccessTime";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Select from "../../components/CustomInputs/Select";
import styles from "./discount-styles.css";
import { saveDiscount, deleteDiscount } from "../../actions/discount";
import SearchProducts from "../SearchProducts";

const materialStyles = theme => ({
  button: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: "6.5rem"
  },
  formControl: {
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    marginTop: theme.spacing.unit,
    width: 230,
    height: 64
  },
  textField: {
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    width: 230,
    fontSize: "0.9rem"
  },
  search: {
    width: 230,
    fontSize: "0.9rem"
  },
  typography: {
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit,
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    fontSize: "0.9rem"
  },
  datePicker: {
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit,
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    width: 230
  },
  paper: {
    backgroundColor: "#303030",
    padding: "1rem"
  }
});

const initialState = {
  typeHasError: false,
  byHasError: false,
  type: "",
  description: "",
  minimumProductQty: "",
  comboProducts: [],
  discountBy: "",
  amount: "",
  percentage: "",
  showSearch: true,
  startDate: null,
  endDate: null,
  startDateHasError: false,
  endDateHasError: false
};

class Inventory extends Component {
  state = initialState;

  typeOptions = [
    {
      label: "Total",
      value: 100
    },
    {
      label: "By Product Quantity",
      value: 101
    },
    {
      label: "Product Combo",
      value: 102
    }
  ];

  discountByOptions = [
    {
      label: "Amount",
      value: "amount"
    },
    {
      label: "Percentage",
      value: "percentage"
    }
  ];

  handleDiscountTypeChange = event => {
    this.setState({
      type: event.target.value,
      typeHasError: false,
      comboProducts: [],
      showSearch: true
    });
  };

  handleDiscountByChange = event => {
    this.setState({
      discountBy: event.target.value,
      byHasError: false
    });
  };

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  selectProduct = product => {
    const { comboProducts, type } = this.state;
    const newComboProducts = [...comboProducts];
    newComboProducts.push(product);

    if (type === 101) {
      this.setState({
        comboProducts: newComboProducts,
        query: "",
        showSearch: false
      });
    } else {
      this.setState({
        comboProducts: newComboProducts,
        query: ""
      });
    }
  };

  save = event => {
    event.preventDefault();
    const discount = { ...this.state };
    const { saveDiscount } = this.props;

    if (!discount.type) {
      this.setState({
        typeHasError: true
      });

      return;
    }

    if (!discount.discountBy) {
      this.setState({
        byHasError: true
      });

      return;
    }

    if (!discount.startDate) {
      this.setState({
        startDateHasError: true
      });

      return;
    }

    if (!discount.endDate) {
      this.setState({
        endDateHasError: true
      });

      return;
    }

    delete discount.typeHasError;
    delete discount.byHasError;
    delete discount.startDateHasError;
    delete discount.endDateHasError;
    delete discount.showSearch;

    saveDiscount(discount);
    this.setState(initialState);
  };

  deleteDiscount = id => {
    this.props.deleteDiscount(id);
  };

  handlePanelChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false
    });
  };

  render() {
    const {
      typeHasError,
      byHasError,
      type,
      comboProducts,
      discountBy,
      showSearch,
      startDate,
      endDate,
      startDateHasError,
      endDateHasError
    } = this.state;
    const { discounts, classes } = this.props;

    return (
      <div className={styles.container}>
        <div className={styles.newDiscount}>
          <Paper square className={classes.paper}>
            <form
              id="discountForm"
              className={styles.formContainer}
              autoComplete="off"
              onSubmit={this.save}
            >
              <Typography className={classes.subheader}>
                New Discount
              </Typography>
              <TextField
                className={classes.textField}
                label="Description"
                id="description"
                onBlur={this.handleChange}
                required
                margin="normal"
              />
              <FormControl className={classes.formControl}>
                <Select
                  label="Select Discount Type"
                  id="type"
                  value={type}
                  onChange={this.handleDiscountTypeChange}
                  options={this.typeOptions}
                />

                {typeHasError && (
                  <FormHelperText>This is required!</FormHelperText>
                )}
              </FormControl>

              <Fragment>
                {type === 100 && (
                  <TextField
                    className={classes.textField}
                    label="Total Bill Amount"
                    id="totalBillAmount"
                    margin="normal"
                    onBlur={this.handleChange}
                    autoFocus
                    required
                  />
                )}

                {(type === 101 || type === 102) && showSearch && (
                  <SearchProducts handleProductClick={this.selectProduct} />
                )}

                {type === 101 && (
                  <div>
                    <div>
                      {comboProducts.map(product => (
                        <Typography
                          className={classes.typography}
                          key={product.id}
                        >
                          Product - {product.name}
                        </Typography>
                      ))}
                    </div>
                    <TextField
                      className={classes.textField}
                      label="Minimum Product Quantity"
                      id="minimumProductQty"
                      onBlur={this.handleChange}
                      required
                      type="number"
                      margin="normal"
                    />
                  </div>
                )}

                {type === 102 && (
                  <div>
                    <Typography className={classes.typography}>
                      Product Combos:
                    </Typography>
                    {comboProducts.map(product => (
                      <Typography
                        className={classes.typography}
                        key={product.id}
                      >
                        {product.name}
                      </Typography>
                    ))}
                  </div>
                )}
              </Fragment>

              <FormControl className={classes.formControl}>
                <Select
                  label="Select Discount By"
                  id="discountBy"
                  value={discountBy}
                  onChange={this.handleDiscountByChange}
                  options={this.discountByOptions}
                />

                {byHasError && (
                  <FormHelperText>This is required!</FormHelperText>
                )}
              </FormControl>

              <TextField
                className={classes.textField}
                label={
                  !discountBy
                    ? "Select Discount By"
                    : discountBy === "amount"
                    ? "Rupees "
                    : "% "
                }
                id={discountBy === "amount" ? "amount " : "percentage"}
                onBlur={this.handleChange}
                required
                type="number"
                margin="normal"
                max={discountBy === "amount" ? 999999 : 100}
                min="0"
              />

              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <DateTimePicker
                  margin="normal"
                  label="Start Date"
                  value={startDate}
                  onChange={date => {
                    this.setState({ startDate: date });
                  }}
                  className={classes.datePicker}
                  dateRangeIcon={<DateRange />}
                  leftArrowIcon={<ChevronLeft />}
                  rightArrowIcon={<ChevronRight />}
                  timeIcon={<AccessTime />}
                />
                {startDateHasError && (
                  <FormHelperText>This is required!</FormHelperText>
                )}
              </MuiPickersUtilsProvider>

              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <DateTimePicker
                  margin="normal"
                  label="End Date"
                  value={endDate}
                  onChange={date => {
                    this.setState({ endDate: date });
                  }}
                  className={classes.datePicker}
                  dateRangeIcon={<DateRange />}
                  leftArrowIcon={<ChevronLeft />}
                  rightArrowIcon={<ChevronRight />}
                  timeIcon={<AccessTime />}
                />
                {endDateHasError && (
                  <FormHelperText>This is required!</FormHelperText>
                )}
              </MuiPickersUtilsProvider>

              <Button
                variant="contained"
                color="primary"
                size="small"
                className={classes.button}
                type="submit"
              >
                Save
              </Button>
            </form>
          </Paper>
        </div>

        <div className={styles.space} />

        {discounts && (
          <div className={styles.discounts}>
            <Paper square className={classes.paper}>
              <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <TableCell align="left">Description</TableCell>
                    <TableCell align="right">Action</TableCell>
                  </TableRow>
                </TableHead>

                <TableBody>
                  {discounts.map(row => (
                    <TableRow hover tabIndex={-1} key={row.id}>
                      <TableCell>{row.description}</TableCell>
                      <TableCell align="right">
                        <Button
                          variant="contained"
                          color="secondary"
                          size="small"
                          className={classes.button}
                          onClick={() => this.deleteDiscount(row.id)}
                        >
                          Delete
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Paper>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  inventories: state.config.data.inventories,
  discounts: state.config.data.discounts
});

const mapDispatchToProps = dispatch => ({
  saveDiscount: discount => dispatch(saveDiscount(discount)),
  deleteDiscount: id => dispatch(deleteDiscount(id))
});

export default withStyles(materialStyles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Inventory)
);
