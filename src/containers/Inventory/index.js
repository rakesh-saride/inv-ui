import React, { Component, Fragment } from "react";
import { Typography } from "@material-ui/core";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Discount from "./Discount";
import styles from "./styles.css";
import ReceivedStock from "./ReceivedStock";

const initialState = {
  expanded: "panel1"
};

class Inventory extends Component {
  state = initialState;

  handlePanelChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false
    });
  };

  render() {
    const { expanded } = this.state;

    return (
      <Fragment>
        <Typography variant="h5">Manage Inventory</Typography>

        <div className={styles.container}>
          <ExpansionPanel
            expanded={expanded === "panel1"}
            onChange={this.handlePanelChange("panel1")}
          >
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography variant="h6">Received Stock</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              {expanded === "panel1" && <ReceivedStock />}
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel
            expanded={expanded === "panel2"}
            onChange={this.handlePanelChange("panel2")}
          >
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography variant="h6">Move Stock</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              {expanded === "panel2" && <ReceivedStock moveStockFlag />}
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel
            expanded={expanded === "panel3"}
            onChange={this.handlePanelChange("panel3")}
          >
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography variant="h6">Discount Setup</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              {expanded === "panel3" && <Discount />}
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>
      </Fragment>
    );
  }
}

export default Inventory;
