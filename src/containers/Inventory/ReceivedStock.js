import React, { Component, Fragment } from "react";
import FormControl from "@material-ui/core/FormControl";
import {
  Paper,
  TextField,
  Button,
  Typography,
  FormHelperText
} from "@material-ui/core";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Select from "../../components/CustomInputs/Select";
import { updateInventory } from "../../actions/product";
import SearchProducts from "../SearchProducts";
import styles from "./received-stock-styles.css";

const materialStyles = theme => ({
  button: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: "6.5rem"
  },
  textField: {
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    width: 230,
    fontSize: "0.9rem"
  },
  paper: {
    backgroundColor: "#303030",
    padding: "1rem"
  },
  formControl: {
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    marginTop: theme.spacing.unit,
    width: 230,
    height: 64
  },
  typography: {
    marginLeft: theme.spacing.unit,
    paddingTop: "2rem"
  }
});

const initialState = {
  fromInventory: "",
  toInventory: "",
  receivedProducts: {},
  inventoryHasError: false
};

class ReceivedStock extends Component {
  state = initialState;

  selectProduct = product => {
    const { receivedProducts } = this.state;
    const newReceivedProducts = {
      ...receivedProducts,
      [product.id]: { name: product.name, quantity: 0 }
    };

    this.setState({
      receivedProducts: newReceivedProducts
    });
  };

  handleChange = (event, id) => {
    const { receivedProducts } = this.state;
    const newReceivedProducts = { ...receivedProducts };
    newReceivedProducts[id].quantity = event.target.value;

    this.setState({
      receivedProducts: newReceivedProducts
    });
  };

  handleChangeForSelect = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  save = event => {
    event.preventDefault();

    const { receivedProducts, fromInventory, toInventory } = this.state;
    const { moveStockFlag, updateInventory } = this.props;

    if (!fromInventory) {
      this.setState({
        inventoryHasError: true
      });

      return;
    }

    if (moveStockFlag && !toInventory) {
      this.setState({
        toInventoryHasError: true
      });

      return;
    }

    if (Object.keys(receivedProducts).length === 0) {
      return;
    }

    const products = [];
    let productQty;
    Object.keys(receivedProducts).forEach(key => {
      productQty = receivedProducts[key].quantity;

      if (moveStockFlag) {
        products.push({
          id: key,
          inventoryInfo: {
            [fromInventory]: -productQty,
            [toInventory]: productQty
          }
        });
      } else {
        products.push({
          id: key,
          inventoryInfo: {
            [fromInventory]: productQty
          }
        });
      }
    });

    updateInventory(products);
    this.setState(initialState);
  };

  render() {
    const {
      receivedProducts,
      fromInventory,
      toInventory,
      inventoryHasError,
      toInventoryHasError
    } = this.state;
    const { inventories, moveStockFlag, classes } = this.props;

    if (inventories.length === 1 && moveStockFlag) {
      return (
        <Typography variant="h6">
          This option is available only if there are more than one inventories.
        </Typography>
      );
    }

    return (
      <div className={styles.container}>
        <div className={styles.select}>
          <Typography className={classes.typography}>
            {moveStockFlag ? "Move stock from " : "Received stock at "}
          </Typography>
          <FormControl className={classes.formControl}>
            <Select
              label="Select Inventory"
              id="fromInventory"
              value={fromInventory}
              onChange={this.handleChangeForSelect}
              options={inventories.map(inv => ({
                value: inv.id,
                label: inv.name
              }))}
            />
            {inventoryHasError && (
              <FormHelperText>This is required!</FormHelperText>
            )}
          </FormControl>
          {moveStockFlag && (
            <Fragment>
              <Typography className={classes.typography}>to</Typography>
              <FormControl className={classes.formControl}>
                <Select
                  label="Select Inventory"
                  id="toInventory"
                  value={toInventory}
                  onChange={this.handleChangeForSelect}
                  options={inventories.map(inv => ({
                    value: inv.id,
                    label: inv.name
                  }))}
                />
                {toInventoryHasError && (
                  <FormHelperText>This is required!</FormHelperText>
                )}
              </FormControl>
            </Fragment>
          )}
        </div>

        <SearchProducts handleProductClick={this.selectProduct} />

        <form
          id="receivedProductsForm"
          autoComplete="off"
          onSubmit={this.save}
          className={styles.tableContainer}
        >
          <Paper square className={classes.paper}>
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell align="left">Product</TableCell>
                  <TableCell align="right">Quantity</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {Object.keys(receivedProducts).map(key => {
                  const product = receivedProducts[key];
                  return (
                    <TableRow hover tabIndex={-1} key={key}>
                      <TableCell>{product.name}</TableCell>
                      <TableCell align="right">
                        <TextField
                          className={classes.textField}
                          label="Quantity"
                          id="quantity"
                          onBlur={event => this.handleChange(event, key)}
                          required
                          margin="normal"
                          type="number"
                          autoFocus
                        />
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </Paper>

          <div>
            <Button
              variant="contained"
              color="primary"
              size="small"
              className={classes.button}
              type="submit"
              disabled={Object.keys(receivedProducts).length === 0}
            >
              Save
            </Button>
            <Button
              variant="contained"
              color="primary"
              size="small"
              className={classes.button}
              onClick={() => {
                this.setState(initialState);
              }}
            >
              Cancel
            </Button>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  inventories: state.config.data.inventories
});

const mapDispatchToProps = dispatch => ({
  updateInventory: products => dispatch(updateInventory(products))
});

export default withStyles(materialStyles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ReceivedStock)
);
