import React, { Component, Fragment } from "react";
import {
  Paper,
  Typography,
  LinearProgress,
  FormHelperText
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Select from "../../components/CustomInputs/Select";
import Product from "../../components/Product";
import {
  searchProducts,
  fetchProductById,
  clearProduct,
  updateInventory,
  deleteProduct
} from "../../actions/product";
import ProductsTable from "./ProductsTable";
import styles from "./styles.css";

const materialStyles = theme => ({
  paper: {
    padding: "1.5rem",
    height: "calc(100vh - 15em)"
  },
  productDetailsPaper: {
    padding: "1.5rem",
    height: "calc(100vh - 18em)",
    overflow: "auto"
  },
  bigButton: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: "9rem"
  },
  button: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: "5rem"
  },
  textField: {
    width: 40,
    fontSize: ".9rem"
  },
  receivedQty: {
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    width: 150,
    fontSize: "1rem"
  },
  formControl: {
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    marginTop: 5,
    marginBottom: 5,
    width: 150
  },
  deleteConfirmation: {
    marginTop: "2rem"
  },
  search: {
    width: "100%"
  }
});

function debounce(func, wait, immediate) {
  let timeout;
  return function() {
    const context = this;
    const args = arguments;
    const later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

class Products extends Component {
  constructor() {
    super();
    this.state = {
      sortBy: "name",
      sortDirection: "asc",
      page: 0,
      selectedRow: null,
      inventoryId: "",
      receivedQuantity: "",
      adjustStockInvId: null,
      newQuantity: "",
      addProductFlag: false,
      tabValue: 0,
      inventoryHasError: false,
      query: ""
    };

    this.searchDebounced = debounce(this.search, 400);
  }

  componentDidMount() {
    this.props.searchProducts("", this.state.page, 10, "", "", "Y");
  }

  handleChange = event => {
    const key = event.target.id;
    if (key.indexOf("-") > -1) {
      const arr = key.split("-");
      this.setState({
        [arr[0]]: {
          ...this.state[arr[0]],
          [arr[1]]: event.target.value
        }
      });
    } else {
      this.setState({
        [event.target.id]: event.target.value
      });
    }
  };

  handleChangeForSelect = event => {
    this.setState({
      inventoryId: event.target.value
    });
  };

  adjustStock = inventoryId => {
    if (!this.state.adjustStockInvId) {
      this.setState({
        adjustStockInvId: inventoryId
      });
    } else {
      const { product, updateInventory } = this.props;
      const { data } = product;
      const { newQuantity, page } = this.state;

      const adjustedInventory = {
        id: data.id,
        inventoryInfo: {
          [inventoryId]:
            parseInt(newQuantity, 10) -
            data.inventoryInfo[inventoryId].availableStock
        }
      };

      updateInventory([adjustedInventory], true, page);

      this.setState({
        adjustStockInvId: null
      });
    }
  };

  receivedStock = event => {
    event.preventDefault();

    const { inventoryId, receivedQuantity, page } = this.state;

    if (!inventoryId) {
      this.setState({
        inventoryHasError: true
      });

      return;
    }

    const { product, updateInventory } = this.props;
    const { data } = product;

    const adjustedInventory = {
      id: data.id,
      inventoryInfo: {
        [inventoryId]: parseInt(receivedQuantity, 10)
      }
    };

    updateInventory([adjustedInventory], true, page);

    this.setState({
      inventoryId: "",
      receivedQuantity: "",
      inventoryHasError: false
    });
  };

  addProduct = () => {
    this.setState({
      addProductFlag: !this.state.addProductFlag
    });
  };

  deleteProduct = event => {
    event.preventDefault();

    const { selectedRow, page } = this.state;
    const { deleteProduct } = this.props;

    deleteProduct(selectedRow, true, page);

    this.setState({
      selectedRow: null
    });
  };

  handleTabChange = (event, tabValue) => {
    this.setState({ tabValue });
  };

  handleSort = (event, property) => {
    const sortBy = property;
    let sortDirection = "desc";

    if (this.state.sortBy === property && this.state.sortDirection === "desc") {
      sortDirection = "asc";
    }

    this.setState({ sortBy, sortDirection });
  };

  handleChangePage = (event, page) => {
    const { searchProducts } = this.props;
    searchProducts("", page, 10, "", "", "Y");
    this.setState({ page });
  };

  handleRowSelect = (event, id) => {
    const { fetchProductById } = this.props;
    fetchProductById(id);
    this.setState({ selectedRow: id });
  };

  search = () => {
    const { query } = this.state;
    const { searchProducts } = this.props;
    if (query.length > 2) {
      searchProducts(query, 0, 10, "", "", "Y");
    }
  };

  handleSearchChange = event => {
    this.setState({ query: event.target.value });

    this.searchDebounced();
  };

  render() {
    const {
      sortBy,
      sortDirection,
      page,
      selectedRow,
      tabValue,
      addProductFlag,
      inventoryId,
      receivedQuantity,
      adjustStockInvId,
      newQuantity,
      inventoryHasError,
      query
    } = this.state;
    const { products, product, inventories, classes } = this.props;

    const { data } = product;

    return (
      <div className={styles.products}>
        <Product open={addProductFlag} close={this.addProduct} />
        <Typography variant="h3">Products</Typography>

        <div className={styles.container}>
          <div className={styles.productTable}>
            <Paper square className={classes.paper}>
              <div className={styles.search}>
                <TextField
                  className={classes.search}
                  label="Search"
                  id="search"
                  margin="dense"
                  value={query}
                  onChange={this.handleSearchChange}
                  autoFocus
                />
                <div className={styles.searchIcon}>
                  <i className="fas fa-search" />
                </div>
              </div>

              {!products.data || products.loading ? (
                <LinearProgress />
              ) : (
                <ProductsTable
                  data={products.data}
                  handleSort={this.handleSort}
                  sortBy={sortBy}
                  sortDirection={sortDirection}
                  page={page}
                  handleChangePage={this.handleChangePage}
                  handleRowSelect={this.handleRowSelect}
                  selectedRow={selectedRow}
                />
              )}
            </Paper>
          </div>
          <div className={styles.productDetails}>
            <Button
              variant="contained"
              color="primary"
              type="submit"
              size="small"
              className={classes.bigButton}
              onClick={this.addProduct}
            >
              Add New Product
            </Button>
            {product.loading ? (
              <LinearProgress />
            ) : product.data ? (
              <Paper square className={classes.productDetailsPaper}>
                <Typography variant="h5" className={styles.productName}>
                  {data.name}
                </Typography>

                <div className={styles.container}>
                  <div>
                    <table className={styles.table}>
                      <tbody>
                        <tr>
                          <td className={styles.labelText}>SKU</td>
                          <td>{data.sku}</td>
                        </tr>
                        <tr>
                          <td className={styles.labelText}>Unit</td>
                          <td>{data.unit}</td>
                        </tr>
                        <tr>
                          <td className={styles.labelText}>Category</td>
                          <td>{data.category}</td>
                        </tr>
                      </tbody>
                    </table>

                    <Typography variant="h6" className={styles.subHeading}>
                      Manufacturer Info
                    </Typography>
                    <table className={styles.table}>
                      <tbody>
                        <tr>
                          <td className={styles.labelText}>Manufacturer</td>
                          <td>{data.manufacturerInfo.manufacturer}</td>
                        </tr>
                        <tr>
                          <td className={styles.labelText}>Brand</td>
                          <td>{data.manufacturerInfo.brand}</td>
                        </tr>
                        <tr>
                          <td className={styles.labelText}>UPC</td>
                          <td>{data.manufacturerInfo.upc}</td>
                        </tr>
                        <tr>
                          <td className={styles.labelText}>ISBN</td>
                          <td>{data.manufacturerInfo.isbn}</td>
                        </tr>
                      </tbody>
                    </table>

                    <Typography variant="h6" className={styles.subHeading}>
                      Sales Info
                    </Typography>
                    <table className={styles.table}>
                      <tbody>
                        <tr>
                          <td className={styles.labelText}>Selling Price</td>
                          <td>{data.salesInfo.sellingPrice}</td>
                        </tr>
                        <tr>
                          <td className={styles.labelText}>Cost Price</td>
                          <td>{data.salesInfo.costPrice}</td>
                        </tr>
                        <tr>
                          <td className={styles.labelText}>Tax</td>
                          <td>{data.salesInfo.tax}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>

                  <div className={styles.inventoryContainer}>
                    <div className={styles.actions}>
                      <Tabs
                        value={tabValue}
                        indicatorColor="primary"
                        textColor="primary"
                        onChange={this.handleTabChange}
                      >
                        <Tab label="Received Stock" />
                        <Tab label="Delete" />
                      </Tabs>

                      {tabValue === 0 && (
                        <form
                          className={styles.formContainer}
                          autoComplete="off"
                          onSubmit={this.receivedStock}
                        >
                          <FormControl className={classes.formControl}>
                            <Select
                              label="Select Inventory"
                              id="inventoryInfo-inventoryId"
                              value={inventoryId}
                              onChange={this.handleChangeForSelect}
                              options={inventories.map(inv => ({
                                value: inv.id,
                                label: inv.name
                              }))}
                            />
                            {inventoryHasError && (
                              <FormHelperText>This is required!</FormHelperText>
                            )}
                          </FormControl>
                          <TextField
                            label="Received Qty"
                            id="receivedQuantity"
                            className={classes.receivedQty}
                            value={receivedQuantity}
                            onChange={this.handleChange}
                            margin="dense"
                            required
                            type="number"
                          />
                          <div>
                            <Button
                              variant="contained"
                              color="primary"
                              type="submit"
                              size="small"
                              className={classes.button}
                              disabled={!inventoryId || !receivedQuantity}
                            >
                              Save
                            </Button>
                            <Button
                              variant="contained"
                              color="primary"
                              size="small"
                              className={classes.button}
                              onClick={() => {
                                this.setState({
                                  inventoryId: "",
                                  receivedQuantity: ""
                                });
                              }}
                            >
                              Cancel
                            </Button>
                          </div>
                        </form>
                      )}

                      {tabValue === 1 && (
                        <form
                          className={styles.formContainer}
                          autoComplete="off"
                          onSubmit={this.deleteProduct}
                        >
                          <Typography className={classes.deleteConfirmation}>
                            Are you sure to delete the product?
                          </Typography>

                          <div>
                            <Button
                              variant="contained"
                              color="primary"
                              type="submit"
                              size="small"
                              className={classes.button}
                            >
                              Yes
                            </Button>
                            <Button
                              variant="contained"
                              color="primary"
                              size="small"
                              className={classes.button}
                              onClick={() => {
                                this.setState({ tabValue: 0 });
                              }}
                            >
                              Cancel
                            </Button>
                          </div>
                        </form>
                      )}
                    </div>

                    <Typography
                      variant="h6"
                      className={styles.inventorySubHeading}
                    >
                      Inventory Info
                    </Typography>
                    <table className={styles.table}>
                      <tbody>
                        {data.inventoryInfo &&
                          Object.keys(data.inventoryInfo).map(invId => (
                            <Fragment key={invId}>
                              <tr>
                                <td className={styles.labelText}>Inventory</td>
                                <td>
                                  {
                                    inventories.filter(
                                      inv => inv.id === invId
                                    )[0].name
                                  }
                                </td>
                                <td />
                              </tr>
                              <tr>
                                <td className={styles.labelText}>
                                  {adjustStockInvId === invId
                                    ? "New Stock\u00A0 \u00A0 \u00A0 \u00A0 \u00A0"
                                    : "Available Stock"}
                                </td>
                                <td>
                                  {adjustStockInvId === invId ? (
                                    <TextField
                                      id="newQuantity"
                                      className={classes.textField}
                                      value={newQuantity}
                                      onChange={this.handleChange}
                                      margin="dense"
                                      required
                                      autoFocus
                                      type="number"
                                    />
                                  ) : (
                                    data.inventoryInfo[invId].availableStock
                                  )}
                                </td>
                                <td>
                                  <Button
                                    variant="contained"
                                    color={
                                      adjustStockInvId === invId
                                        ? "primary"
                                        : "secondary"
                                    }
                                    onClick={() => this.adjustStock(invId)}
                                    size="small"
                                    className={classes.bigButton}
                                    disabled={
                                      adjustStockInvId === invId && !newQuantity
                                    }
                                  >
                                    {adjustStockInvId === invId
                                      ? "Update"
                                      : "Adjust Stock"}
                                  </Button>
                                </td>
                              </tr>
                            </Fragment>
                          ))}
                      </tbody>
                    </table>
                  </div>
                </div>
              </Paper>
            ) : (
              <Paper square className={classes.productDetailsPaper}>
                <Typography variant="h5" className={styles.productName}>
                  Select a product on the left
                </Typography>
              </Paper>
            )}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.products,
  product: state.product,
  inventories: state.config.data.inventories
});

const mapDispatchToProps = dispatch => ({
  searchProducts: (
    query,
    page,
    size,
    sortDirection,
    sortBy,
    inventoryInfoOnly
  ) =>
    dispatch(
      searchProducts(
        query,
        page,
        size,
        sortDirection,
        sortBy,
        inventoryInfoOnly
      )
    ),
  fetchProductById: id => dispatch(fetchProductById(id)),
  updateInventory: (products, refetchProductsFlag, page) =>
    dispatch(updateInventory(products, refetchProductsFlag, page)),
  deleteProduct: (id, refetchProductsFlag, page) =>
    dispatch(deleteProduct(id, refetchProductsFlag, page)),
  clearProduct: () => dispatch(clearProduct())
});

export default withStyles(materialStyles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Products)
);
