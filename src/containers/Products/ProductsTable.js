import React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Tooltip from "@material-ui/core/Tooltip";
import { withStyles } from "@material-ui/core/styles";

const headerRows = [
  { id: "name", label: "Product Name" },
  {
    id: "stockOnHand",
    numeric: true,
    label: "Stock On Hand"
  }
];

const styles = {
  paper: {
    width: "100%",
    height: "95%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between"
  },
  tableWrapper: {
    overflowX: "auto"
  },
  selectedRow: {
    backgroundColor: "#52b202 !important"
  }
};

class ProductsTable extends React.Component {
  isSelected = id => this.props.selectedRow === id;

  createSortHandler = property => event => {
    this.props.handleSort(event, property);
  };

  render() {
    const {
      data,
      sortBy,
      sortDirection,
      page,
      handleChangePage,
      handleRowSelect,
      classes
    } = this.props;

    const rows = [];
    data.forEach(product => {
      let stockOnHand = 0;
      if (product.inventoryInfo)
        Object.keys(product.inventoryInfo).forEach(inv => {
          stockOnHand += product.inventoryInfo[inv].availableStock;
        });

      rows.push({
        id: product.id,
        name: product.name,
        stockOnHand
      });
    });

    return (
      <div className={classes.paper}>
        <div className={classes.tableWrapper}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                {headerRows.map(
                  row => (
                    <TableCell
                      key={row.id}
                      align={row.numeric ? "right" : "left"}
                      sortDirection={sortBy === row.id ? sortDirection : false}
                    >
                      <Tooltip
                        title="Sort"
                        placement={row.numeric ? "bottom-end" : "bottom-start"}
                        enterDelay={300}
                      >
                        <TableSortLabel
                          active={sortBy === row.id}
                          direction={sortDirection}
                          onClick={this.createSortHandler(row.id)}
                        >
                          {row.label}
                        </TableSortLabel>
                      </Tooltip>
                    </TableCell>
                  ),
                  this
                )}
              </TableRow>
            </TableHead>

            <TableBody>
              {rows.map(row => {
                const isSelected = this.isSelected(row.id);
                return (
                  <TableRow
                    hover
                    onClick={event => handleRowSelect(event, row.id)}
                    tabIndex={-1}
                    key={row.id}
                    className={isSelected ? classes.selectedRow : null}
                  >
                    <TableCell component="th" scope="row">
                      {row.name}
                    </TableCell>
                    <TableCell align="right">{row.stockOnHand}</TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          component="div"
          count={data.length}
          rowsPerPage={10}
          page={page}
          backIconButtonProps={{
            "aria-label": "Previous Page"
          }}
          nextIconButtonProps={{
            "aria-label": "Next Page"
          }}
          onChangePage={handleChangePage}
          rowsPerPageOptions={[]}
        />
      </div>
    );
  }
}

export default withStyles(styles)(ProductsTable);
