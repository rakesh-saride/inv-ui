import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import { withStyles } from "@material-ui/core/styles";
import DiscountLineItem from "../DiscountLineItem";
import styles from "./styles.css";

const materialStyles = theme => ({
  button: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: "6.5rem"
  },
  input: {
    fontSize: "inherit"
  }
});

class EditLineItem extends Component {
  state = {
    quantity: null,
    price: null
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      quantity:
        nextProps.selectedProducts.length === 1
          ? nextProps.products[nextProps.selectedProducts[0]].quantity
          : null,
      price:
        nextProps.selectedProducts.length === 1
          ? nextProps.products[nextProps.selectedProducts[0]].price
          : null
    });
  }

  handlePriceChange = event => {
    if (event.key === "Enter") {
      this.props.handlePriceChange(this.state.price);
    }
  };

  handleQtyChange = event => {
    if (event.key === "Enter") {
      this.props.handleQtyChange(this.state.quantity);
    }
  };

  handleInputQtyChange = event => {
    this.setState({ quantity: event.target.value });
  };

  handleInputPriceChange = event => {
    this.setState({ price: event.target.value });
  };

  getAppliedDiscounts = (discountsApplied, selectedProducts, products) => {
    if (discountsApplied.length === 0) {
      return "";
    }

    if (selectedProducts.length === 1) {
      return discountsApplied[selectedProducts[0]];
    }

    let diffDiscounts = [];
    if (selectedProducts.length > 0) {
      let discountLine;
      for (let i = 0; i < selectedProducts.length; i++) {
        discountLine = selectedProducts[i];
        if (diffDiscounts.length === 0) {
          diffDiscounts = [...discountsApplied[discountLine]];
        } else {
          if (discountsApplied[discountLine].length !== diffDiscounts.length) {
            return "The line products have different discounts applied, select them individually to remove discounts";
          }
          discountsApplied[discountLine].forEach(d => {
            if (!(diffDiscounts.indexOf(d) > -1)) {
              return "The line products have different discounts applied, select them individually to remove discounts";
            }
          });
        }
      }
    } else if (products.length === discountsApplied.length) {
      let discountLine;
      for (let i = 0; i < products.length; i++) {
        discountLine = i;
        if (diffDiscounts.length === 0) {
          diffDiscounts = [...discountsApplied[discountLine]];
        } else {
          if (discountsApplied[discountLine].length !== diffDiscounts.length) {
            return "The line products have different discounts applied, select them individually to remove discounts";
          }
          discountsApplied[discountLine].forEach(d => {
            if (!(diffDiscounts.indexOf(d) > -1)) {
              return "The line products have different discounts applied, select them individually to remove discounts";
            }
          });
        }
      }
    } else {
      return "The line products have different discounts applied, select them individually to remove discounts";
    }

    return diffDiscounts;
  };

  render() {
    const {
      products,
      selectedProducts,
      discountsApplied,
      discountTypes,
      handleApplyDiscount,
      handleDeleteDiscount,
      resetTabToSearch,
      classes
    } = this.props;

    const { quantity, price } = this.state;

    return (
      <div>
        {selectedProducts.length === 1 && (
          <div className={styles.container}>
            <table className={styles.editTable}>
              <tbody>
                <tr>
                  <td className={styles.editTableCellFixedWidth}>
                    Description
                  </td>
                  <td className={styles.editTableCell}>This is description</td>
                </tr>
                <tr>
                  <td className={styles.editTableCellFixedWidth}>Quantity</td>
                  <td className={styles.editTableCell}>
                    <TextField
                      value={quantity}
                      onChange={this.handleInputQtyChange}
                      margin="dense"
                      type="number"
                      step="1"
                      className={classes.input}
                      onKeyPress={this.handleQtyChange}
                    />
                  </td>
                </tr>
                <tr>
                  <td className={styles.editTableCellFixedWidth}>Price</td>
                  <td className={styles.editTableCell}>
                    <TextField
                      value={price}
                      onChange={this.handleInputPriceChange}
                      margin="dense"
                      type="number"
                      step="any"
                      className={classes.input}
                      onKeyPress={this.handlePriceChange}
                    />
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        )}

        <div className={styles.container}>
          <DiscountLineItem
            selectedProducts={selectedProducts}
            discountsApplied={this.getAppliedDiscounts(
              discountsApplied,
              selectedProducts,
              products
            )}
            handleApplyDiscount={handleApplyDiscount}
            handleDeleteDiscount={handleDeleteDiscount}
            discountTypes={discountTypes}
            resetTabToSearch={resetTabToSearch}
          />
        </div>
      </div>
    );
  }
}

export default withStyles(materialStyles)(EditLineItem);
