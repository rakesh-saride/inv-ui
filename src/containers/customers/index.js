import { Typography, Button } from "@material-ui/core";
import React, { Component } from "react";
import { Paper } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import { withStyles } from "@material-ui/core/styles";
import CustomerTable from "../../components/CustomerTable";
import styles from "./customer-styles.css";
import AddCustomer from "../../components/AddCustomer";

const materialStyles = theme => ({
  paper: {
    padding: "1.5rem",
    height: "calc(100vh - 15em)"
  },
  customerDetailsPaper: {
    padding: "1.5rem",
    height: "calc(100vh - 18em)",
    overflow: "auto"
  },
  bigButton: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: "9rem"
  },
  button: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: "5rem"
  },
  textField: {
    width: 40,
    fontSize: ".9rem"
  },
  formControl: {
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    marginTop: 5,
    marginBottom: 5,
    width: 150
  },
  deleteConfirmation: {
    marginTop: "2rem"
  },
  search: {
    width: "100%"
  }
});

class Customers extends Component {
  state = {
    addCustomerFlag: false
  };

  addCustomer = () => {
    this.setState({
      addCustomerFlag: !this.state.addCustomerFlag
    });
  };

  render() {
    const { addCustomerFlag } = this.state;

    const { classes } = this.props;

    return (
      <div className={styles.customers}>
        <Typography variant="h3">Customers</Typography>
        <div className={styles.container}>
          <div className={styles.customerTable}>
            <Paper square className={classes.paper}>
              <div className={styles.search}>
                <TextField
                  className={classes.search}
                  label="Search"
                  id="search"
                  margin="dense"
                  autoFocus
                />
                <div className={styles.searchIcon}>
                  <i className="fas fa-search" />
                </div>
              </div>
              <CustomerTable />
            </Paper>
          </div>
          <div className={styles.customerDetails}>
            <Button
              variant="contained"
              color="primary"
              type="submit"
              size="small"
              className={classes.bigButton}
              onClick={this.addCustomer}
            >
              Add Customer
            </Button>
            <AddCustomer open={addCustomerFlag} close={this.addCustomer} />
            <Paper square className={classes.customerDetailsPaper}>
              <Typography variant="h5" className={styles.customerName}>
                Select a customer on the left
              </Typography>
              <Typography variant="h5">Customer Details</Typography>
              <div className={styles.container}>
                <div>
                  <table className={styles.table}>
                    <tbody>
                      <tr>
                        <td className={styles.labelText}>FirstName</td>
                        <td>firstname</td>
                      </tr>
                      <tr>
                        <td className={styles.labelText}>LastName</td>
                        <td>lastname</td>
                      </tr>
                      <tr>
                        <td className={styles.labelText}>Phone</td>
                        <td>phone</td>
                      </tr>
                      <tr>
                        <td className={styles.labelText}>Email</td>
                        <td>email</td>
                      </tr>
                      <tr>
                        <td className={styles.labelText}>Address</td>
                        <td>address</td>
                      </tr>
                      <tr />
                      <tr>
                        <td>
                          <Button
                            variant="contained"
                            color="primary"
                            size="small"
                            onClick={this.editCustomer}
                          >
                            Edit
                          </Button>
                        </td>
                        <td>
                          <Button
                            variant="contained"
                            color="primary"
                            size="small"
                            onClick={this.deleteCustomer}
                          >
                            Delete
                          </Button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </Paper>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(materialStyles)(Customers);
