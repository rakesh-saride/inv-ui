import React, { Component } from "react";
import Barchart from "../../components/Barchart";

export default class Dashboard extends Component {
  render() {
    return (
      <div style={{ maxWidth: "30rem", backgroundColor: "#fff" }}>
        <Barchart />
      </div>
    );
  }
}
