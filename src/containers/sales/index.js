import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import List from "@material-ui/core/List";
import ListSubheader from "@material-ui/core/ListSubheader";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import { Typography } from "@material-ui/core";
import MediaQuery from "react-responsive";
import { withStyles } from "@material-ui/core/styles";
import EditLineItem from "../EditLineItem";
import LineItemsTable from "../../components/LineItemsTable";
import MobileView from "../../components/LineItemsTable/MobileView";
import styles from "./sales-styles.css";

const materialStyles = {
  paper: {
    width: "100%",
    height: "calc(100vh - 12em)"
  }
};

class Home extends Component {
  state = {
    products: [],
    selectedProduct: null,
    selectedProducts: [],
    open: false,
    value: 2,
    discountTypes: {
      233: { description: "Customer Satisfaction", percentage: "10" },
      234: { description: "Damaged", percentage: "20" }
    },
    discountsApplied: [],
    customerId: null,
    productsInCart: []
  };

  componentDidMount() {
    this.setState({
      products: this.getRows()
    });
  }

  handleTabChange = (event, value) => {
    this.setState({ value });
  };

  resetTabToSearch = () => {
    this.setState({ value: 0 });
  };

  priceRow = (quantity, unit) => quantity * unit;

  createRow = (id, description, quantity, unit) => {
    const price = this.priceRow(quantity, unit);
    return { id, description, quantity, unit, price };
  };

  products = [
    ["Paperclips (Box)", 100, 1.15],
    ["Paper (Case)", 10, 45.99],
    ["Waste Basket", 2, 17.99]
  ].map((product, id) => this.createRow(id, ...product));

  getRows = () => this.products;

  handleRowClick = index => {
    this.setState({
      selectedProduct: index
    });
  };

  handleTableKeyNavigation = event => {
    const keyCode = event.keyCode;
    // Enter
    if (keyCode == 13) {
      if (selectedProduct || selectedProduct == 0) {
        if (changeQty) {
          this.setState(
            {
              changeQty: !changeQty
            },
            () => {
              ReactDOM.findDOMNode(this.refs.tableBody).focus();
            }
          );
        } else {
          this.setState({
            changeQty: !changeQty
          });
        }
      }
    }
    // Up
    if (keyCode == 38) {
      if (selectedProduct || selectedProduct === 0) {
        if (selectedProduct == 0) {
          this.setState({
            selectedProduct: totalRows - 1
          });
        } else {
          this.setState({
            selectedProduct: selectedProduct - 1
          });
        }
      }
    }
    // Down
    if (keyCode == 40) {
      if (selectedProduct || selectedProduct === 0) {
        if (selectedProduct + 1 == totalRows) {
          this.setState({
            selectedProduct: 0
          });
        } else {
          this.setState({
            selectedProduct: selectedProduct + 1
          });
        }
      }
    }
    // Delete
    if (keyCode == 46) {
      // TODO delete logic
    }
  };

  handleClickOnLineItem = id => {
    const { selectedProducts } = this.state;
    const selectedIndex = selectedProducts.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selectedProducts, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selectedProducts.slice(1));
    } else if (selectedIndex === selectedProducts.length - 1) {
      newSelected = newSelected.concat(selectedProducts.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selectedProducts.slice(0, selectedIndex),
        selectedProducts.slice(selectedIndex + 1)
      );
    }

    this.setState({ selectedProducts: newSelected });
  };

  handleSelectAllClick = event => {
    if (event.target.checked) {
      const selectedProducts = [];

      this.state.products.forEach((product, index) => {
        selectedProducts.push(index);
      });

      this.setState({
        selectedProducts
      });
    } else {
      this.setState({ selectedProducts: [] });
    }
  };

  handleQtyChange = quantity => {
    const changedItem = this.state.products[this.state.selectedProducts[0]];
    changedItem.quantity = quantity;

    const changedItems = this.state.products;
    changedItems[this.state.selectedProducts[0]] = changedItem;

    this.setState({
      products: changedItems
    });
  };

  handlePriceChange = price => {
    const changedItem = this.state.products[this.state.selectedProducts[0]];
    changedItem.price = parseFloat(price);

    const changedItems = this.state.products;
    changedItems[this.state.selectedProducts[0]] = changedItem;

    this.setState({
      products: changedItems
    });
  };

  handleApplyDiscount = discountObj => {
    let newDiscounts;

    if (discountObj.overrideExistingDiscounts) {
      newDiscounts = [];
    } else {
      newDiscounts = [...this.state.discountsApplied];
    }

    if (discountObj.applyToAllLines) {
      this.state.products.forEach((product, index) => {
        if (newDiscounts[index]) {
          newDiscounts[index] = [
            ...newDiscounts[index],
            discountObj.selectedDiscountCode
          ];
        } else {
          newDiscounts[index] = [discountObj.selectedDiscountCode];
        }
      });
    } else {
      this.state.selectedProducts.forEach(itemIndex => {
        if (newDiscounts[itemIndex]) {
          newDiscounts[itemIndex] = [
            ...newDiscounts[itemIndex],
            discountObj.selectedDiscountCode
          ];
        } else {
          newDiscounts[itemIndex] = [discountObj.selectedDiscountCode];
        }
      });
    }

    this.setState({
      discountsApplied: newDiscounts
    });
  };

  handleDeleteDiscount = discountCode => {
    const discounts = [...this.state.discountsApplied];

    if (this.state.selectedProducts && this.state.selectedProducts.length > 0) {
      discounts[this.state.selectedProducts].splice(
        discounts[this.state.selectedProducts].indexOf(discountCode),
        1
      );
    } else {
      this.state.products.forEach((product, index) => {
        discounts[index].splice(discounts[index].indexOf(discountCode), 1);
      });
    }

    this.setState({
      discountsApplied: discounts
    });
  };

  handleDeleteLineItem = () => {
    const itemIndexes = Array.from(this.state.products.keys());

    this.state.selectedProducts.forEach(selectedIndex => {
      delete itemIndexes[selectedIndex];
    });

    const products = [];

    for (const index in itemIndexes) {
      products.push(this.state.products[index]);
    }

    this.setState({
      products,
      selectedProducts: []
    });
  };

  render() {
    const { classes } = this.props;
    const {
      products,
      selectedProducts,
      open,
      value,
      discountTypes,
      discountsApplied
    } = this.state;

    return (
      <div className={styles.sales}>
        <Typography variant="h3" className={styles.header}>
          Sales
        </Typography>

        <div className={styles.body}>
          <div className={styles.searchContainer}>
            <Paper square className={classes.paper}>
              <Tabs
                value={value}
                indicatorColor="primary"
                textColor="primary"
                onChange={this.handleTabChange}
                fullWidth
              >
                <Tab label="Search" />
                <Tab label="Browse" />
                <Tab label="Edit" />
              </Tabs>

              {value === 2 && (
                <EditLineItem
                  products={products}
                  selectedProducts={selectedProducts}
                  handleQtyChange={this.handleQtyChange}
                  handlePriceChange={this.handlePriceChange}
                  discountsApplied={discountsApplied}
                  handleApplyDiscount={this.handleApplyDiscount}
                  handleDeleteDiscount={this.handleDeleteDiscount}
                  discountTypes={discountTypes}
                  resetTabToSearch={this.resetTabToSearch}
                />
              )}

              {value === 1 && (
                <div className={styles.categoryContainer}>
                  <List component="nav" className={styles.categoryNav}>
                    <ListSubheader>
                      <Typography variant="h6">Categories</Typography>
                    </ListSubheader>
                    <Divider />
                    <ListItem button>
                      <ListItemText>
                        <Typography>Groceries</Typography>
                      </ListItemText>
                    </ListItem>
                    <ListItem button>
                      <ListItemText>
                        <Typography>Tea</Typography>
                      </ListItemText>
                    </ListItem>
                    <ListItem
                      button
                      onClick={() => {
                        this.setState({ open: !open });
                      }}
                    >
                      <ListItemText>
                        <Typography>Chocolates</Typography>
                      </ListItemText>
                      {!open ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                      <List component="div" disablePadding>
                        <ListItem button style={{ paddingLeft: "2.5rem" }}>
                          <ListItemText>
                            <Typography>Eclairs</Typography>
                          </ListItemText>
                        </ListItem>
                      </List>
                    </Collapse>
                    <ListItem button>
                      <ListItemText>
                        <Typography>Frozen</Typography>
                      </ListItemText>
                    </ListItem>
                  </List>

                  <div className={styles.categoryResults}>
                    <Paper square>
                      <div className={(styles.results, styles.transition)}>
                        <div className={styles.result}>
                          <div className={styles.title}>
                            Tetly Green Tea 100 Bags
                            <div className={styles.description}>TATA Tetly</div>
                          </div>
                          <div className={styles.column}>
                            <div className={styles.price}>68.00</div>
                            <img
                              alt=""
                              src="https://images-eu.ssl-images-amazon.com/images/I/41-2x+3x4gL._AC_US218_FMwebp_QL70_.jpg"
                            />
                          </div>
                        </div>
                        <div className={styles.result}>
                          <div className={styles.title}>
                            Tetly Ginger Tea 50 Bags
                            <div className={styles.description}>TATA Tetly</div>
                          </div>
                          <div className={styles.column}>
                            <div className={styles.price}>55.00</div>
                          </div>
                        </div>
                      </div>
                    </Paper>
                  </div>
                </div>
              )}
              {value === 0 && (
                <div className={styles.searchWrapper}>
                  <div className={styles.search}>
                    <div className={styles.searchIcon}>
                      <i className="fas fa-search" />
                    </div>
                    <div className={styles.searchInput}>
                      <TextField
                        label="Search"
                        margin="dense"
                        style={{ width: "95%" }}
                      />
                    </div>
                  </div>

                  <Paper square className={styles.menu}>
                    <div className={(styles.results, styles.transition)}>
                      <div className={styles.result}>
                        <div className={styles.title}>
                          Tetly Green Tea 100 Bags
                          <div className={styles.description}>TATA Tetly</div>
                        </div>
                        <div className={styles.column}>
                          <div className={styles.price}>68.00</div>
                          <img
                            alt=""
                            src="https://images-eu.ssl-images-amazon.com/images/I/41-2x+3x4gL._AC_US218_FMwebp_QL70_.jpg"
                          />
                        </div>
                      </div>
                      <div className={styles.result}>
                        <div className={styles.title}>
                          Tetly Ginger Tea 50 Bags
                          <div className={styles.description}>TATA Tetly</div>
                        </div>
                        <div className={styles.column}>
                          <div className={styles.price}>55.00</div>
                        </div>
                      </div>
                    </div>
                  </Paper>
                </div>
              )}
            </Paper>
          </div>
          <div className={styles.productsContainer}>
            <MediaQuery minWidth={681}>
              <LineItemsTable
                selectedProducts={selectedProducts}
                handleSelectAllClick={this.handleSelectAllClick}
                products={products}
                handleClickOnLineItem={this.handleClickOnLineItem}
                handleDeleteLineItem={this.handleDeleteLineItem}
              />
            </MediaQuery>
            <MediaQuery maxWidth={680}>
              <MobileView
                selectedProducts={selectedProducts}
                handleSelectAllClick={this.handleSelectAllClick}
                products={products}
                handleClickOnLineItem={this.handleClickOnLineItem}
              />
            </MediaQuery>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(materialStyles)(Home);
