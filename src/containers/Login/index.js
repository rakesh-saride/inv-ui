import React, { Component, Fragment } from "react";
import { Redirect } from "react-router-dom";
import {
  Paper,
  Typography,
  Avatar,
  FormControl,
  Input,
  InputLabel,
  Button,
  LinearProgress,
  Snackbar,
  IconButton,
  SnackbarContent
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import green from "@material-ui/core/colors/green";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { login } from "../../actions/login";
import styles from "./login-styles.css";
import { closeSnackbar } from "../../actions/snackbar";

const materialStyles = theme => ({
  button: {
    marginTop: "1rem",
    width: "6rem"
  },
  signup: {
    marginTop: "2rem"
  },
  success: {
    backgroundColor: green[600]
  },
  error: {
    backgroundColor: theme.palette.error.dark
  },
  message: {
    display: "flex",
    alignItems: "center"
  }
});

class Login extends Component {
  state = {
    username: "",
    password: ""
  };

  handleChange = event =>
    this.setState({ [event.target.name]: event.target.value });

  submit = event => {
    event.preventDefault();

    const { userDetails, login } = this.props;

    if (userDetails.loading) return;

    login(this.state);
  };

  render() {
    const { userDetails, snackbar, closeSnackbar, classes } = this.props;

    if (userDetails.data.firstName) {
      return <Redirect to="/" />;
    }

    return (
      <Fragment>
        {userDetails.loading && <LinearProgress color="secondary" />}
        <div className={styles.login}>
          <Paper className={styles.body}>
            <Avatar className={styles.avatar}>
              <i className="fas fa-user-lock" />
            </Avatar>
            <Typography component="h1" variant="h5">
              Sign in
            </Typography>
            <form className={styles.loginForm} onSubmit={this.submit}>
              <FormControl margin="normal" required fullWidth>
                <InputLabel htmlFor="username">Username</InputLabel>
                <Input
                  id="username"
                  name="username"
                  autoComplete="username"
                  autoFocus
                  onChange={this.handleChange}
                  required
                />
              </FormControl>
              <FormControl margin="normal" required fullWidth>
                <InputLabel htmlFor="password">Password</InputLabel>
                <Input
                  name="password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  onChange={this.handleChange}
                  required
                />
              </FormControl>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                disabled={userDetails.loading}
                className={classes.button}
              >
                Sign in
              </Button>
            </form>
            <Typography variant="subtitle1" className={classes.signup}>
              New User? <a href="/signup">Click here to signup.</a>
            </Typography>
          </Paper>
        </div>

        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          open={snackbar.open}
          onClose={closeSnackbar}
          autoHideDuration={2000}
        >
          <SnackbarContent
            className={classes[snackbar.variant]}
            aria-describedby="client-snackbar"
            message={
              <span id="client-snackbar" className={classes.message}>
                {snackbar.message}
              </span>
            }
            action={[
              <IconButton
                key="close"
                aria-label="Close"
                color="inherit"
                onClick={closeSnackbar}
              >
                <CloseIcon />
              </IconButton>
            ]}
          />
        </Snackbar>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  userDetails: state.userDetails,
  snackbar: state.snackbar
});

const mapDispatchToProps = dispatch => ({
  login: credentials => dispatch(login(credentials)),
  closeSnackbar: () => dispatch(closeSnackbar())
});

export default withStyles(materialStyles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Login)
);
