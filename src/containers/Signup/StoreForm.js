import React from "react";
import {
  Typography,
  TextField,
  Grid,
  Divider,
  Button
} from "@material-ui/core";

function StoreForm(props) {
  const { handleSignup, handleChange, singupLoadingStatus } = props;
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Store details
      </Typography>
      <form autoComplete="off" onSubmit={handleSignup}>
        <Grid container spacing={24}>
          <Grid item xs={12} md={6}>
            <TextField
              required
              id="storeInfo-storeName"
              label="Store Name"
              onBlur={handleChange}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              required
              id="storeInfo-address1"
              name="address1"
              label="Address line 1"
              autoComplete="address-line1"
              onBlur={handleChange}
              fullWidth
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="storeInfo-address2"
              name="address2"
              label="Address line 2"
              autoComplete="address-line2"
              onBlur={handleChange}
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="storeInfo-city"
              name="city"
              label="City"
              autoComplete="address-level2"
              onBlur={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="storeInfo-state"
              name="state"
              label="State"
              onBlur={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="storeInfo-zip"
              name="zip"
              label="Zip / Postal code"
              autoComplete="billing postal-code"
              onBlur={handleChange}
            />
          </Grid>
          <Divider />
          <Grid item xs={12} md={12}>
            <TextField
              required
              id="storeInfo-inventoryName"
              label="Primary Inventory Name"
              onBlur={handleChange}
            />
          </Grid>
        </Grid>
        <div
          style={{
            display: "flex",
            justifyContent: "flex-end",
            margin: "2rem 0rem"
          }}
        >
          {props.activeStep !== 0 && (
            <Button onClick={props.handleBack} style={{ marginRight: 8 }}>
              Back
            </Button>
          )}
          <Button
            variant="contained"
            color="primary"
            type="submit"
            disabled={singupLoadingStatus}
          >
            Sign up
          </Button>
        </div>
      </form>
    </React.Fragment>
  );
}

export default StoreForm;
