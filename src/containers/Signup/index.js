import React from "react";
import {
  CssBaseline,
  Paper,
  Stepper,
  Step,
  StepLabel,
  Typography
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import UserForm from "./UserForm";
import StoreForm from "./StoreForm";
import { signup } from "../../actions/signup";

const materialStyles = theme => ({
  layout: {
    width: "auto",
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    [theme.breakpoints.up(600 + theme.spacing.unit * 2 * 2)]: {
      width: 600,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 3,
    padding: theme.spacing.unit * 2,
    [theme.breakpoints.up(600 + theme.spacing.unit * 3 * 2)]: {
      marginTop: theme.spacing.unit * 6,
      marginBottom: theme.spacing.unit * 6,
      padding: theme.spacing.unit * 3
    }
  },
  stepper: {
    padding: `${theme.spacing.unit * 3}px 0 ${theme.spacing.unit * 5}px`
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end"
  },
  button: {
    marginTop: theme.spacing.unit * 3,
    marginLeft: theme.spacing.unit
  }
});

const steps = ["User details", "Store details"];

class Signup extends React.Component {
  state = {
    activeStep: 0
  };

  handleNext = () => {
    this.setState(state => ({
      activeStep: state.activeStep + 1
    }));
  };

  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1
    }));
  };

  handleReset = () => {
    this.setState({
      activeStep: 0
    });
  };

  handleChange = event => {
    if (event.target.id === "confirmPassword") {
      if (this.state.password !== event.target.value) {
        this.setState({
          confirmPasswordHasError: true,
          confirmPassword: event.target.value
        });
      } else {
        this.setState({
          confirmPasswordHasError: false,
          confirmPassword: event.target.value
        });
      }

      return;
    }

    const key = event.target.id;
    if (key.indexOf("-") > -1) {
      const arr = key.split("-");
      this.setState({
        [arr[0]]: {
          ...this.state[arr[0]],
          [arr[1]]: event.target.value
        }
      });
    } else {
      this.setState({
        [event.target.id]: event.target.value
      });
    }
  };

  handleSignup = event => {
    event.preventDefault();

    if (this.props.signupDetails.loading) {
      return;
    }

    const credentials = { ...this.state };

    delete credentials.confirmPasswordHasError;
    delete credentials.confirmPassword;
    delete credentials.activeStep;

    this.props.signup(credentials);
  };

  getStepContent = step => {
    switch (step) {
      case 0:
        return (
          <UserForm
            handleNext={this.handleNext}
            handleChange={this.handleChange}
            confirmPasswordHasError={this.state.confirmPasswordHasError}
          />
        );
      case 1:
        return (
          <StoreForm
            handleBack={this.handleBack}
            handleChange={this.handleChange}
            handleSignup={this.handleSignup}
            singupLoadingStatus={this.props.signupDetails.loading}
          />
        );
      default:
        throw new Error("Unknown step");
    }
  };

  render() {
    const { signupDetails, classes } = this.props;
    const { activeStep } = this.state;

    return (
      <React.Fragment>
        <CssBaseline />

        <main className={classes.layout}>
          <Paper className={classes.paper}>
            <Typography component="h1" variant="h4" align="center">
              {!signupDetails.data && "Signup"}
            </Typography>

            <React.Fragment>
              {signupDetails.data ? (
                <React.Fragment>
                  <Typography variant="h5" gutterBottom>
                    Signed up succesfully.
                  </Typography>
                  <Typography variant="subtitle1">
                    Please <a href="/login">click here</a> to login.
                  </Typography>
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <Stepper activeStep={activeStep} className={classes.stepper}>
                    {steps.map(label => (
                      <Step key={label}>
                        <StepLabel>{label}</StepLabel>
                      </Step>
                    ))}
                  </Stepper>
                  {this.getStepContent(activeStep)}
                </React.Fragment>
              )}
            </React.Fragment>
          </Paper>
        </main>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  signupDetails: state.signupDetails
});

const mapDispatchToProps = dispatch => ({
  signup: credentials => dispatch(signup(credentials))
});

export default withStyles(materialStyles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Signup)
);
