import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import { Button, FormControl, FormHelperText } from "@material-ui/core";

function UserForm(props) {
  const { handleNext, handleChange, confirmPasswordHasError } = props;

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        User details
      </Typography>
      <form autoComplete="off" onSubmit={handleNext}>
        <Grid container spacing={24}>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="firstName"
              name="firstName"
              label="First name"
              autoComplete="fname"
              onBlur={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="lastName"
              name="lastName"
              label="Last name"
              autoComplete="lname"
              onBlur={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={12}>
            <TextField
              required
              id="username"
              name="username"
              label="Username"
              onBlur={handleChange}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="password"
              name="password"
              label="Password"
              type="password"
              onBlur={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormControl>
              <TextField
                required
                id="confirmPassword"
                name="confirmPassword"
                label="Confirm Password"
                type="password"
                onChange={handleChange}
              />

              {confirmPasswordHasError && (
                <FormHelperText>Passwords do not match!</FormHelperText>
              )}
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="phoneNumber"
              name="phoneNumber"
              label="Phone Number"
              type="number"
              max="9999999999"
              onBlur={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="email"
              name="email"
              label="Email"
              type="email"
              onBlur={handleChange}
            />
          </Grid>
        </Grid>
        <div
          style={{
            display: "flex",
            justifyContent: "flex-end",
            margin: "2rem 0rem"
          }}
        >
          <Button variant="contained" color="primary" type="submit">
            Next
          </Button>
        </div>
      </form>
    </React.Fragment>
  );
}

export default UserForm;
