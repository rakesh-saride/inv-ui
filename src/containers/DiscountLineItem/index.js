import React, { Component } from "react";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import { Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles.css";

const materialStyles = theme => ({
  button: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: "6.5rem"
  },
  formControl: {
    minWidth: 120
  },
  discountAppliedText: {
    fontSize: "1rem",
    margin: "auto 0"
  },
  differentDiscountsText: {
    fontSize: "1rem",
    margin: "1rem 0"
  },
  header: {
    fontSize: "1.1rem",
    textAlign: "center"
  }
});

class DiscountLineItem extends Component {
  state = {
    selectedDiscountCode: 0,
    overrideExistingDiscounts: false,
    applyToAllLines: false
  };

  handleDiscountChange = event => {
    this.setState({ selectedDiscountCode: event.target.value });
  };

  handleOverrideExistingDiscounts = () => {
    this.setState({
      overrideExistingDiscounts: !this.state.overrideExistingDiscounts
    });
  };

  handleApplyToAllLines = () => {
    this.setState({
      applyToAllLines: !this.state.applyToAllLines
    });
  };

  handleApply = () => {
    this.setState({
      selectedDiscountCode: 0,
      overrideExistingDiscounts: false,
      applyToAllLines: false
    });
    this.props.handleApplyDiscount(this.state);
  };

  render() {
    const {
      selectedProducts,
      discountTypes,
      discountsApplied,
      handleDeleteDiscount,
      resetTabToSearch,
      classes
    } = this.props;

    const {
      selectedDiscountCode,
      overrideExistingDiscounts,
      applyToAllLines
    } = this.state;

    return (
      <div className={styles.discountContainer}>
        {Array.isArray(discountsApplied) && discountsApplied.length > 0 ? (
          <div className={styles.discountsAppliedContainer}>
            <Typography className={classes.header}>
              {selectedProducts.length > 0
                ? "Discounts applied"
                : "Discounts applied to all products"}
            </Typography>
            {discountsApplied.map((discount, index) => (
              <div key={index} className={styles.discountApplied}>
                <Typography className={classes.discountAppliedText}>
                  {discountTypes[discount].description}
                </Typography>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => handleDeleteDiscount(discount)}
                  size="small"
                  className={classes.button}
                >
                  Delete
                </Button>
              </div>
            ))}
          </div>
        ) : (
          <Typography className={classes.differentDiscountsText}>
            {discountsApplied}
          </Typography>
        )}

        <Divider />
        <Typography className={classes.header}>Apply new discount</Typography>
        <table className={styles.editTable}>
          <tbody>
            <tr>
              <td colSpan={2}>
                <FormControl className={classes.formControl}>
                  <InputLabel htmlFor="discount-selection">
                    Select Discount
                  </InputLabel>
                  <Select
                    value={selectedDiscountCode}
                    onChange={this.handleDiscountChange}
                    input={<Input name="discount" id="discount-selection" />}
                  >
                    <MenuItem value={0}>
                      <em>None</em>
                    </MenuItem>
                    {Object.keys(discountTypes).map(dc => {
                      const discountType = discountTypes[dc];
                      return (
                        <MenuItem value={dc} key={dc}>
                          {`${discountType.description} ${
                            discountType.percentage
                          }%`}
                        </MenuItem>
                      );
                    })}
                  </Select>
                </FormControl>
              </td>
            </tr>
            <tr>
              <td className={styles.editTableCellFixedWidth}>
                Override existing discounts
              </td>
              <td className={styles.editTableCell}>
                <Checkbox
                  checked={overrideExistingDiscounts}
                  onChange={this.handleOverrideExistingDiscounts}
                  value="overrideExistingDiscounts"
                  color="secondary"
                />
              </td>
            </tr>
            <tr>
              <td className={styles.editTableCellFixedWidth}>
                Apply to all lines
              </td>
              <td>
                <Checkbox
                  checked={applyToAllLines}
                  onChange={this.handleApplyToAllLines}
                  value="applyToAllLines"
                  color="secondary"
                />
              </td>
            </tr>
          </tbody>
        </table>
        <div className={styles.buttonContainer}>
          <Button
            variant="contained"
            color="secondary"
            onClick={this.handleApply}
            size="small"
            className={classes.button}
            disabled={
              selectedDiscountCode === 0 ||
              (!applyToAllLines && selectedProducts.length === 0)
            }
          >
            Apply
          </Button>
          <Button
            variant="contained"
            color="secondary"
            onClick={resetTabToSearch}
            size="small"
            className={classes.button}
          >
            Cancel
          </Button>
        </div>
      </div>
    );
  }
}

export default withStyles(materialStyles)(DiscountLineItem);
