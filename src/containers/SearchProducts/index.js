import React, { Component } from "react";
import { TextField, CircularProgress, Paper } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { searchProducts, clearProducts } from "../../actions/product";
import styles from "./styles.css";

const materialStyles = {
  search: {
    width: 230,
    fontSize: "0.9rem"
  },
  searchResults: {
    position: "absolute",
    zIndex: "100",
    minWidth: 230
  }
};

function debounce(func, wait, immediate) {
  let timeout;
  return function() {
    const context = this;
    const args = arguments;
    const later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

class SearchProduct extends Component {
  constructor() {
    super();
    this.state = {
      query: ""
    };

    this.searchDebounced = debounce(this.search, 400);
  }

  componentDidMount() {
    this.props.clearProducts();
  }

  componentWillUnmount() {
    this.props.clearProducts();
  }

  search = () => {
    const { query } = this.state;
    const { searchProducts } = this.props;
    if (query.length > 2) {
      searchProducts(query, 0, 10, "", "", "Y");
    }
  };

  handleSearchChange = event => {
    this.setState({ query: event.target.value });

    this.searchDebounced();
  };

  handleClick = product => {
    const { handleProductClick, clearProducts } = this.props;

    if (product) {
      handleProductClick(product);
    }
    clearProducts();
    this.setState({
      query: ""
    });
  };

  render() {
    const { products, classes } = this.props;
    const { query } = this.state;

    return (
      <div className={styles.searchWrapper}>
        <div className={styles.search}>
          <TextField
            className={classes.search}
            label="Search"
            id="search"
            margin="dense"
            value={query}
            onChange={this.handleSearchChange}
            autoFocus
          />
          <div className={styles.searchIcon}>
            {products.loading ? (
              <CircularProgress size={22} color="secondary" />
            ) : (
              <i className="fas fa-search" />
            )}
          </div>
        </div>

        <Paper square className={classes.searchResults}>
          <div className={(styles.results, styles.transition)}>
            {products.data &&
              (products.data.length === 0 ? (
                <div
                  className={styles.result}
                  onClick={() => this.handleClick()}
                >
                  <div className={styles.title}>
                    No Results
                    <div className={styles.description} />
                  </div>
                </div>
              ) : (
                products.data.map(product => (
                  <div
                    key={product.id}
                    className={styles.result}
                    onClick={() => this.handleClick(product)}
                  >
                    <div className={styles.title}>
                      {product.name}
                      <div className={styles.description}>
                        {product.manufacturer} {product.brand}
                      </div>
                    </div>
                  </div>
                ))
              ))}
          </div>
        </Paper>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.products
});

const mapDispatchToProps = dispatch => ({
  searchProducts: (
    query,
    page,
    size,
    sortDirection,
    sortBy,
    inventoryInfoOnly
  ) =>
    dispatch(
      searchProducts(
        query,
        page,
        size,
        sortDirection,
        sortBy,
        inventoryInfoOnly
      )
    ),
  clearProducts: () => dispatch(clearProducts())
});

export default withStyles(materialStyles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(SearchProduct)
);
