import React, { Component, Fragment } from "react";
import axios from "axios";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { Link, Route, Redirect, Switch } from "react-router-dom";
import {
  Snackbar,
  LinearProgress,
  IconButton,
  SnackbarContent
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import green from "@material-ui/core/colors/green";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import reduxStore from "../../store";
import Products from "../Products";
import Sales from "../Sales";
import Inventory from "../Inventory";
import {
  fetchUnits,
  fetchDiscounts,
  fetchInventoryDetails
} from "../../actions/config";
import { fetchCategories } from "../../actions/product";
import { logout } from "../../actions/login";
import { closeSnackbar } from "../../actions/snackbar";

const materialStyles = theme => ({
  grow: {
    flexGrow: 1
  },
  success: {
    backgroundColor: green[600]
  },
  error: {
    backgroundColor: theme.palette.error.dark
  }
});

class Home extends Component {
  constructor(props) {
    super(props);

    axios.defaults.headers.common.Authorization =
      props.userDetails.data.authorizationToken;

    const UNAUTHORIZED = 401;
    const FORBIDDEN = 403;
    const { dispatch } = reduxStore;
    axios.interceptors.response.use(
      response => response,
      error => {
        const { status } = error.response;
        if (status === UNAUTHORIZED || status === FORBIDDEN) {
          dispatch(logout());
        }
        return Promise.reject(error);
      }
    );

    if (props.userDetails.data.authorizationToken) {
      props.fetchUnits();
      props.fetchDiscounts();
      props.fetchInventoryDetails();
      props.fetchCategories();
    }
  }

  computeClassName = toPath => {
    return this.props.location.pathname === toPath ? "link active" : "link";
  };

  render() {
    const {
      config,
      categories,
      userDetails,
      snackbar,
      logout,
      closeSnackbar,
      classes
    } = this.props;

    if (!userDetails.data.authorizationToken) {
      return <Redirect to="/login" />;
    }

    if (
      !config.data ||
      config.loading ||
      !categories.data ||
      categories.loading
    ) {
      return <LinearProgress color="secondary" />;
    }

    return (
      <Fragment>
        <AppBar position="static" color="primary">
          <Toolbar>
            <Typography variant="h6" className={classes.grow}>
              Inv.
            </Typography>

            <Link className={this.computeClassName("/sales")} to="/sales">
              <Button>Sales</Button>
            </Link>
            <Link className={this.computeClassName("/products")} to="/products">
              <Button>Products</Button>
            </Link>
            <Link
              className={this.computeClassName("/inventory")}
              to="/inventory"
            >
              <Button>Inventory</Button>
            </Link>
            <Button
              style={{ marginLeft: "3rem" }}
              onClick={() => {
                logout();
              }}
            >
              Logout
            </Button>
          </Toolbar>
        </AppBar>

        <Switch>
          <Route path="/sales" component={Sales} />
          <Route path="/products" component={Products} />
          <Route path="/customers" component={Products} />
          <Route path="/inventory" component={Inventory} />
          <Route path="/" component={Products} />
          <Redirect to="/sales" />
        </Switch>

        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          open={snackbar.open}
          onClose={closeSnackbar}
          autoHideDuration={2000}
        >
          <SnackbarContent
            className={classes[snackbar.variant]}
            aria-describedby="client-snackbar"
            message={
              <span id="client-snackbar" className={classes.message}>
                {snackbar.message}
              </span>
            }
            action={[
              <IconButton
                key="close"
                aria-label="Close"
                color="inherit"
                onClick={closeSnackbar}
              >
                <CloseIcon />
              </IconButton>
            ]}
          />
        </Snackbar>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  config: state.config,
  categories: state.categories,
  userDetails: state.userDetails,
  snackbar: state.snackbar
});

const mapDispatchToProps = dispatch => ({
  fetchUnits: () => dispatch(fetchUnits()),
  fetchDiscounts: () => dispatch(fetchDiscounts()),
  fetchInventoryDetails: () => dispatch(fetchInventoryDetails()),
  fetchCategories: () => dispatch(fetchCategories()),
  logout: () => dispatch(logout()),
  closeSnackbar: () => dispatch(closeSnackbar())
});

export default withStyles(materialStyles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Home)
);
