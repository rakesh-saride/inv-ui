import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import { render } from "react-dom";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import blue from "@material-ui/core/colors/blue";
import { LinearProgress } from "@material-ui/core";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/lib/integration/react";
import store, { persistor } from "./store";
import App from "./containers/App";

import "./global-styles/base.scss";
import "./assets/images/favicon.ico";

const theme = createMuiTheme({
  palette: {
    type: "dark",
    primary: blue,
    secondary: {
      light: "#87e547",
      main: "#52b202",
      dark: "#0b8100",
      contrastText: "#ffffff"
    }
  },
  typography: {
    useNextVariants: true,
    // Use the Open Sans font instead of the default Roboto font.
    fontFamily: ["Open Sans", "Helvetica", "Arial", "sans-serif"].join(",")
  },
  overrides: {
    MuiPaper: {
      root: {
        backgroundColor: "#212121"
      }
    },
    MuiTableCell: {
      root: {
        userSelect: "none",
        padding: "4px 24px 4px 24px"
      }
    },
    MuiInputBase: {
      root: {
        fontSize: "inherit"
      },
      input: {
        padding: "6px 0 3px",
        fontSize: "0.9rem"
      }
    },
    MuiFormLabel: {
      root: {
        fontSize: "0.9rem"
      }
    },
    MuiMenuItem: {
      root: {
        fontSize: "0.9rem"
      }
    },
    MuiFormHelperText: {
      root: {
        color: "#87e547"
      }
    },
    MuiDialog: {
      paper: {
        margin: 8
      }
    }
  }
});

render(
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
      <PersistGate
        loading={<LinearProgress color="secondary" />}
        persistor={persistor}
      >
        <BrowserRouter>
          <Route path="/" component={App} />
        </BrowserRouter>
      </PersistGate>
    </Provider>
  </MuiThemeProvider>,
  document.querySelector("#app")
);
