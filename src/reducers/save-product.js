import {
  SAVE_PRODUCT_LOADING,
  SAVE_PRODUCT_SUCCESS,
  SAVE_PRODUCT_ERROR
} from "../constants/actions";

const initialState = {
  loading: false,
  data: null,
  err: null
};

export default function saveProduct(state = initialState, action) {
  switch (action.type) {
    case SAVE_PRODUCT_LOADING:
      return { ...state, loading: true };
    case SAVE_PRODUCT_SUCCESS:
      return { ...state, loading: false, data: action.data };
    case SAVE_PRODUCT_ERROR:
      return { ...state, loading: false, err: action.err };

    default:
      return state;
  }
}
