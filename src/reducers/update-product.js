import {
  UPDATE_INVENTORY_LOADING,
  UPDATE_INVENTORY_SUCCESS,
  UPDATE_INVENTORY_ERROR
} from "../constants/actions";

const initialState = {
  loading: false,
  data: null,
  err: null
};

export default function updateProduct(state = initialState, action) {
  switch (action.type) {
    case UPDATE_INVENTORY_LOADING:
      return { ...state, loading: true };
    case UPDATE_INVENTORY_SUCCESS:
      return { ...state, loading: false, data: action.data };
    case UPDATE_INVENTORY_ERROR:
      return { ...state, loading: false, err: action.err };

    default:
      return state;
  }
}
