import {
  SAVE_DISCOUNT_LOADING,
  SAVE_DISCOUNT_SUCCESS,
  SAVE_DISCOUNT_ERROR
} from "../constants/actions";

const initialState = {
  loading: false,
  data: null,
  err: null
};

export default function saveProduct(state = initialState, action) {
  switch (action.type) {
    case SAVE_DISCOUNT_LOADING:
      return { ...state, loading: true };
    case SAVE_DISCOUNT_SUCCESS:
      return { ...state, loading: false, data: action.data };
    case SAVE_DISCOUNT_ERROR:
      return { ...state, loading: false, err: action.err };

    default:
      return state;
  }
}
