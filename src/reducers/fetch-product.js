import {
  FETCH_PRODUCT_LOADING,
  FETCH_PRODUCT_SUCCESS,
  FETCH_PRODUCT_ERROR,
  CLEAR_PRODUCT
} from "../constants/actions";

const initialState = {
  loading: false,
  data: null,
  err: null
};

export default function fetchProduct(state = initialState, action) {
  switch (action.type) {
    case FETCH_PRODUCT_LOADING:
      return { ...state, loading: true };
    case FETCH_PRODUCT_SUCCESS:
      return { ...state, loading: false, data: action.data };
    case FETCH_PRODUCT_ERROR:
      return { ...state, loading: false, err: action.err };
    case CLEAR_PRODUCT:
      return initialState;

    default:
      return state;
  }
}
