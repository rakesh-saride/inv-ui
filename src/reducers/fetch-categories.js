import {
  FETCH_CATEGORIES_LOADING,
  FETCH_CATEGORIES_SUCCESS,
  FETCH_CATEGORIES_ERROR
} from "../constants/actions";

const initialState = {
  loading: false,
  data: null,
  err: null
};

export default function fetchCategories(state = initialState, action) {
  switch (action.type) {
    case FETCH_CATEGORIES_LOADING:
      return { ...state, loading: true };
    case FETCH_CATEGORIES_SUCCESS:
      return { ...state, loading: false, data: action.data };
    case FETCH_CATEGORIES_ERROR:
      return { ...state, loading: false, err: action.err };

    default:
      return state;
  }
}
