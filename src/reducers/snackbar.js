import { OPEN_SNACKBAR, CLOSE_SNACKBAR } from "../constants/actions";

const initialState = {
  open: false,
  variant: "success",
  closeHandler: null,
  message: ""
};

export default function snackbar(state = initialState, action) {
  switch (action.type) {
    case OPEN_SNACKBAR:
      return { ...state, ...action.data };
    case CLOSE_SNACKBAR:
      return { initialState };

    default:
      return state;
  }
}
