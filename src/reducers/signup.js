import {
  SIGNUP_LOADING,
  SIGNUP_SUCCESS,
  SIGNUP_ERROR
} from "../constants/actions";

const initialState = {
  loading: false,
  data: null,
  err: null
};

export default function login(state = initialState, action) {
  switch (action.type) {
    case SIGNUP_LOADING:
      return { ...state, loading: true };
    case SIGNUP_SUCCESS:
      return { ...state, loading: false, data: action.data };
    case SIGNUP_ERROR:
      return { ...state, loading: false, err: action.err };

    default:
      return state;
  }
}
