import {
  FETCH_UNITS_LOADING,
  FETCH_UNITS_SUCCESS,
  FETCH_UNITS_ERROR,
  FETCH_DISCOUNTS_LOADING,
  FETCH_DISCOUNTS_SUCCESS,
  FETCH_DISCOUNTS_ERROR,
  FETCH_INVENTORY_LOADING,
  FETCH_INVENTORY_SUCCESS,
  FETCH_INVENTORY_ERROR
} from "../constants/actions";

const initialState = {
  loading: false,
  data: null,
  err: null
};

export default function config(state = initialState, action) {
  switch (action.type) {
    case FETCH_UNITS_LOADING:
      return { ...state, loading: true };
    case FETCH_UNITS_SUCCESS:
      return {
        ...state,
        loading: false,
        data: {
          ...state.data,
          units: action.data
        }
      };
    case FETCH_UNITS_ERROR:
      return { ...state, loading: false, err: action.err };
    case FETCH_DISCOUNTS_LOADING:
      return { ...state, loading: true };
    case FETCH_DISCOUNTS_SUCCESS:
      return {
        ...state,
        loading: false,
        data: {
          ...state.data,
          discounts: action.data
        }
      };
    case FETCH_DISCOUNTS_ERROR:
      return { ...state, loading: false, err: action.err };
    case FETCH_INVENTORY_LOADING:
      return { ...state, loading: true };
    case FETCH_INVENTORY_SUCCESS:
      return {
        ...state,
        loading: false,
        data: {
          ...state.data,
          inventories: action.data
        }
      };
    case FETCH_INVENTORY_ERROR:
      return { ...state, loading: false, err: action.err };

    default:
      return state;
  }
}
