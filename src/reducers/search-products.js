import {
  SEARCH_PRODUCTS_LOADING,
  SEARCH_PRODUCTS_SUCCESS,
  SEARCH_PRODUCTS_ERROR,
  CLEAR_PRODUCTS
} from "../constants/actions";

const initialState = {
  loading: false,
  data: null,
  err: null
};

export default function searchProduct(state = initialState, action) {
  switch (action.type) {
    case SEARCH_PRODUCTS_LOADING:
      return { ...state, loading: true };
    case SEARCH_PRODUCTS_SUCCESS:
      return { ...state, loading: false, data: action.data };
    case SEARCH_PRODUCTS_ERROR:
      return { ...state, loading: false, err: action.err };
    case CLEAR_PRODUCTS:
      return { initialState };

    default:
      return state;
  }
}
