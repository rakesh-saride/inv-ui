import {
  LOGIN_LOADING,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGOUT
} from "../constants/actions";

const initialState = {
  loading: false,
  data: { authorizationToken: null },
  err: null
};

export default function login(state = initialState, action) {
  switch (action.type) {
    case LOGIN_LOADING:
      return { ...state, loading: true };
    case LOGIN_SUCCESS:
      return { ...state, loading: false, data: action.data };
    case LOGIN_ERROR:
      return { ...state, loading: false, err: action.err };

    default:
      return state;
  }
}
