import {
  DELETE_PRODUCT_LOADING,
  DELETE_PRODUCT_SUCCESS,
  DELETE_PRODUCT_ERROR
} from "../constants/actions";

const initialState = {
  loading: false,
  data: null,
  err: null
};

export default function deleteProduct(state = initialState, action) {
  switch (action.type) {
    case DELETE_PRODUCT_LOADING:
      return { ...state, loading: true };
    case DELETE_PRODUCT_SUCCESS:
      return { ...state, loading: false };
    case DELETE_PRODUCT_ERROR:
      return { ...state, loading: false, err: action.err };

    default:
      return state;
  }
}
