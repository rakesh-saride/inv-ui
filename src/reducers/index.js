import { combineReducers } from "redux";
import saveProduct from "./save-product";
import saveDiscount from "./save-discount";
import config from "./config";
import searchProduct from "./search-products";
import fetchProduct from "./fetch-product";
import fetchCategories from "./fetch-categories";
import snackbar from "./snackbar";
import login from "./login";
import updateProduct from "./update-product";
import deleteProduct from "./delete-product";
import signup from "./signup";
import { LOGOUT } from "../constants/actions";

const appReducer = combineReducers({
  savedProduct: saveProduct,
  savedDiscount: saveDiscount,
  products: searchProduct,
  product: fetchProduct,
  categories: fetchCategories,
  snackbar,
  config,
  userDetails: login,
  updateProduct,
  deleteProduct,
  signupDetails: signup
});

const rootReducer = (state, action) => {
  if (action.type === LOGOUT) {
    state = undefined;
  }

  return appReducer(state, action);
};

export default rootReducer;
