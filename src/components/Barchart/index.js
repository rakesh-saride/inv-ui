import React from "react";
import { VictoryChart, VictoryTheme, VictoryBar } from "victory";

const sampleData = [
  { quarter: 1, earnings: 13000 },
  { quarter: 2, earnings: 16500 },
  { quarter: 3, earnings: 14250 },
  { quarter: 4, earnings: 19000 }
];

export default () => {
  return (
    <VictoryChart theme={VictoryTheme.material} domainPadding={{ x: 15 }}>
      <VictoryBar
        barRatio={0.8}
        style={{
          data: { fill: "#c43a31" }
        }}
        data={sampleData}
        x="quarter"
        // data accessor for y values
        y="earnings"
      />
    </VictoryChart>
  );
};
