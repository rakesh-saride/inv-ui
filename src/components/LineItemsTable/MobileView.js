import React from "react";
import Paper from "@material-ui/core/Paper";
import { Typography } from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";
import Divider from "@material-ui/core/Divider";
import { withStyles } from "@material-ui/core/styles";
import styles from "./mobile-styles.css";

const materialStyles = theme => ({
  quantity: {
    color: "rgba(255, 255, 255, 0.7)",
    fontSize: "0.75rem"
  }
});

const MobileView = props => {
  const { selectedProducts, products, handleClickOnLineItem, classes } = props;

  const isRowSelected = id => selectedProducts.indexOf(id) !== -1;

  const ccyFormat = num => `${num.toFixed(2)}`;

  const subtotal = products =>
    products.map(({ price }) => price).reduce((sum, i) => sum + i, 0);

  const TAX_RATE = 0.07;

  const invoiceSubtotal = subtotal(products);
  const invoiceTaxes = TAX_RATE * invoiceSubtotal;
  const invoiceTotal = invoiceTaxes + invoiceSubtotal;

  return (
    <Paper square>
      <div>
        {products
          ? products.map((product, index) => {
              const isSelected = isRowSelected(index);
              return (
                <div className={styles.row}>
                  <Checkbox checked={isSelected} />
                  <div className={styles.product}>
                    <div>
                      <Typography>{product.description}</Typography>
                      <Typography className={classes.quantity}>
                        Quantity: {product.quantity}
                      </Typography>
                    </div>
                    <Typography>{ccyFormat(product.price)}</Typography>
                  </div>
                  <Divider />
                </div>
              );
            })
          : null}
      </div>
    </Paper>
  );
};

export default withStyles(materialStyles)(MobileView);
