import React from "react";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Checkbox from "@material-ui/core/Checkbox";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import { Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";

const materialStyles = {
  paper: {
    width: "100%",
    height: "calc(100vh - 12em)",
    overflow: "auto"
  },
  spacer: {
    flex: "1 1"
  }
};

let EnhancedTableToolbar = props => {
  const { numSelected, handleDeleteLineItem, classes } = props;

  return (
    <Toolbar>
      <div>
        {numSelected > 0 ? (
          <Typography variant="subtitle1">{numSelected} selected</Typography>
        ) : (
          <Typography variant="h6">Items</Typography>
        )}
      </div>
      <div className={classes.spacer} />
      <div>
        {numSelected > 0 ? (
          <IconButton aria-label="Delete" onClick={handleDeleteLineItem}>
            <DeleteIcon />
          </IconButton>
        ) : null}
      </div>
    </Toolbar>
  );
};

EnhancedTableToolbar = withStyles(materialStyles)(EnhancedTableToolbar);

const LineItemsTable = props => {
  const {
    selectedProducts,
    handleSelectAllClick,
    products,
    handleClickOnLineItem,
    handleDeleteLineItem,
    classes
  } = props;

  const isRowSelected = id => selectedProducts.indexOf(id) !== -1;

  const ccyFormat = num => `${num.toFixed(2)}`;
  const subtotal = () =>
    products.map(({ price }) => price).reduce((sum, i) => sum + i, 0);

  const TAX_RATE = 0.07;

  const invoiceSubtotal = subtotal(products);
  const invoiceTaxes = TAX_RATE * invoiceSubtotal;
  const invoiceTotal = invoiceTaxes + invoiceSubtotal;

  return (
    <Paper square className={classes.paper}>
      <div>
        <EnhancedTableToolbar
          numSelected={selectedProducts.length}
          handleDeleteLineItem={handleDeleteLineItem}
        />
        <Table>
          <TableHead>
            <TableRow>
              <TableCell padding="checkbox">
                <Checkbox
                  indeterminate={
                    selectedProducts.length > 0 &&
                    selectedProducts.length < products.length
                  }
                  checked={
                    selectedProducts.length > 0 &&
                    selectedProducts.length === products.length
                  }
                  onChange={handleSelectAllClick}
                />
              </TableCell>
              <TableCell>Desc</TableCell>
              <TableCell align="right">Qty.</TableCell>
              <TableCell align="right">@</TableCell>
              <TableCell align="right">Price</TableCell>
            </TableRow>
          </TableHead>
          <TableBody style={{ outline: "none" }}>
            {products
              ? products.map((product, index) => {
                  const isSelected = isRowSelected(index);
                  return (
                    <TableRow
                      hover
                      onClick={() => handleClickOnLineItem(index)}
                      role="checkbox"
                      aria-checked={isSelected}
                      tabIndex={-1}
                      selected={isSelected}
                      key={product.id}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox checked={isSelected} />
                      </TableCell>
                      <TableCell>{product.description}</TableCell>
                      <TableCell align="right">{product.quantity}</TableCell>
                      <TableCell align="right">{product.unit}</TableCell>
                      <TableCell align="right">
                        {ccyFormat(product.price)}
                      </TableCell>
                    </TableRow>
                  );
                })
              : null}
            <TableRow>
              <TableCell rowSpan={3} />
              <TableCell rowSpan={3} />
              <TableCell colSpan={2}>Subtotal</TableCell>
              <TableCell align="right">{ccyFormat(invoiceSubtotal)}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Tax</TableCell>
              <TableCell align="right">{`${(TAX_RATE * 100).toFixed(
                0
              )} %`}</TableCell>
              <TableCell align="right">{ccyFormat(invoiceTaxes)}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell colSpan={2}>Total</TableCell>
              <TableCell align="right">{ccyFormat(invoiceTotal)}</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    </Paper>
  );
};

export default withStyles(materialStyles)(LineItemsTable);
