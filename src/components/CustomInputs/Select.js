import React, { Component, Fragment } from "react";
import { InputLabel, Select, Input, MenuItem } from "@material-ui/core";

export default class CustomSelect extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.value === this.props.value) {
      return false;
    }

    return true;
  }

  render() {
    const { label, options, ...remainingProps } = this.props;

    return (
      <Fragment>
        <InputLabel htmlFor={`${remainingProps.id}-helper`}>{label}</InputLabel>
        <Select
          {...remainingProps}
          input={
            <Input
              name={remainingProps.id}
              id={`${remainingProps.id}-helper`}
            />
          }
        >
          {options.map(option =>
            option.value ? (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ) : (
              <MenuItem key={option} value={option}>
                {option}
              </MenuItem>
            )
          )}
        </Select>
      </Fragment>
    );
  }
}
