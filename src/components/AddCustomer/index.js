import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { Paper, Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles.css";

const materialStyles = theme => ({
  paper: {
    padding: "1rem"
  },
  textField: {
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    width: 230,
    fontSize: "0.9rem"
  },
  formControl: {
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    marginTop: theme.spacing.unit,
    width: 230,
    height: 64
  },
  button: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: "6.5rem"
  }
});

const initialState = {
  firstName: "",
  lastName: "",
  phoneNumber: "",
  email: "",
  address: ""
};

class AddCustomer extends Component {
  state = initialState;

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  save = event => {
    event.preventDefault();
    const customer = { ...this.state };
    console.log(customer);
    this.setState(initialState);
    this.props.close();
  };

  handleCancel = () => {
    this.setState(initialState);
    this.props.close();
  };

  render() {
    const { classes, open, close } = this.props;

    return (
      <Dialog
        open={open}
        onClose={() => close()}
        disableBackdropClick
        aria-labelledby="New Customer"
        maxWidth="md"
      >
        <div className={styles.container}>
          <Paper square className={classes.paper}>
            <Typography variant="h5">New Customer</Typography>
            <form
              id="customerDetails"
              className={styles.formContainer}
              autoComplete="off"
              onSubmit={this.save}
            >
              <TextField
                label="First Name"
                id="firstName"
                className={classes.textField}
                onBlur={this.handleChange}
                margin="normal"
                required
              />
              <TextField
                label="Last Name"
                id="lastName"
                className={classes.textField}
                onBlur={this.handleChange}
                margin="normal"
                required
              />
              <TextField
                label="Phone Number"
                id="phone"
                className={classes.textField}
                onBlur={this.handleChange}
                margin="normal"
                type="number"
                required
              />
              <TextField
                label="Email"
                id="email"
                className={classes.textField}
                onBlur={this.handleChange}
                margin="normal"
              />
              <TextField
                label="Address"
                onBlur={this.handleChange}
                className={classes.textField}
                id="address"
                margin="normal"
              />
            </form>
            <div className={styles.buttonContainer}>
              <Button
                variant="contained"
                color="primary"
                size="small"
                className={classes.button}
                type="submit"
              >
                Save
              </Button>
              <Button
                variant="contained"
                color="primary"
                onClick={this.handleCancel}
                size="small"
                className={classes.button}
              >
                Cancel
              </Button>
            </div>
          </Paper>
        </div>
      </Dialog>
    );
  }
}

export default withStyles(materialStyles)(AddCustomer);
