import React from "react";
import PropTypes from "prop-types";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { withStyles } from "@material-ui/core/styles";

const materialStyles = theme => ({
  paper: {
    width: "100%",
    height: "95%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between"
  },
  tableWrapper: {
    overflowX: "auto"
  },
  selectedRow: {
    backgroundColor: "#52b202 !important"
  }
});

const CustomerTableCell = withStyles(theme => ({
  head: {
    fontSize: 14,
    fontWeight: "bold",
    textAlign: "center"
  },
  body: {
    fontSize: 12,
    textAlign: "center"
  }
}))(TableCell);

let id = 0;
function createData(name, phone) {
  id += 1;
  return { id, name, phone };
}

const rows = [
  createData("Sowmya", 9849741789),
  createData("Ricky", 9393036723),
  createData("Eclair", 8143137891)
];

const CustomerTable = props => {
  const { classes } = props;

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <CustomerTableCell>Customer Name</CustomerTableCell>
            <CustomerTableCell numeric>Phone</CustomerTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => {
            return (
              <TableRow className={classes.row} key={row.id}>
                <CustomerTableCell component="th" scope="row">
                  {row.name}
                </CustomerTableCell>
                <CustomerTableCell numeric>{row.phone}</CustomerTableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
  );
};

CustomerTable.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(materialStyles)(CustomerTable);
