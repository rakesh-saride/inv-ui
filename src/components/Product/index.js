import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { Paper, Typography, FormHelperText } from "@material-ui/core";
import Chip from "@material-ui/core/Chip";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Select from "../CustomInputs/Select";
import { saveProduct } from "../../actions/product";
import styles from "./styles.css";
import ReactSelect from "../ReactSelect";

const materialStyles = theme => ({
  paper: {
    padding: "1rem"
  },
  textField: {
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    width: 230,
    fontSize: "0.9rem"
  },
  formControl: {
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    marginTop: theme.spacing.unit,
    width: 230,
    height: 64
  },
  divider: {
    margin: "1.5rem 0rem 1.5rem 0em"
  },
  subHeader: {
    fontSize: "1rem"
  },
  button: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: "6.5rem"
  },
  addButton: {
    marginTop: theme.spacing.unit * 2,
    marginLeft: theme.spacing.unit * 2
  }
});

const initialState = {
  name: "",
  sku: "",
  unit: "",
  category: "",
  tag: "",
  tags: [],
  manufacturerInfo: {
    manufacturer: "",
    brand: "",
    isbn: "",
    upc: ""
  },
  salesInfo: {
    sellingPrice: "",
    costPrice: "",
    tax: ""
  },
  inventoryInfo: {
    inventoryId: "",
    availableStock: "",
    reorderLevel: ""
  },
  expanded: "panel1",
  unitHasError: false,
  categoryHasError: false
};

class Product extends Component {
  state = initialState;

  handleChange = event => {
    const key = event.target.id;
    if (key.indexOf("-") > -1) {
      const arr = key.split("-");
      this.setState({
        [arr[0]]: {
          ...this.state[arr[0]],
          [arr[1]]: event.target.value
        }
      });
    } else {
      this.setState({
        [event.target.id]: event.target.value
      });
    }
  };

  handleChangeForSelect = event => {
    const key = event.target.name;
    if (key.indexOf("-") > -1) {
      const arr = key.split("-");
      this.setState({
        [arr[0]]: {
          ...this.state[arr[0]],
          [arr[1]]: event.target.value
        },
        unitHasError: false
      });
    } else {
      this.setState({
        [event.target.name]: event.target.value,
        unitHasError: false
      });
    }
  };

  handleChangeOfReactSelect = option => {
    if (option) {
      this.setState({
        category: option,
        categoryHasError: false
      });
    } else {
      this.setState({
        category: ""
      });
    }
  };

  save = event => {
    event.preventDefault();
    const product = { ...this.state };
    const { saveProduct, close } = this.props;

    if (product.inventoryInfo.inventoryId) {
      product.inventoryInfo = {
        [product.inventoryInfo.inventoryId]: {
          ...product.inventoryInfo
        }
      };
    } else {
      delete product.inventoryInfo;
    }

    product.category = product.category.value;

    delete product.tag;
    delete product.options;
    delete product.unitHasError;
    delete product.categoryHasError;
    delete product.expanded;

    saveProduct(product, true);
    this.setState(initialState);
    close();
  };

  addTag = () => {
    const { tag, tags } = this.state;
    const newTags = [...tags];
    newTags.push(tag);

    this.setState({
      tags: newTags
    });
  };

  handleTagDelete = tagName => {
    const { tags } = this.state;
    const newTags = [...tags];

    newTags.splice(tags.indexOf(tagName), 1);

    this.setState({
      tags: newTags
    });
  };

  getChip = tagName => (
    <Chip
      key={tagName}
      label={tagName}
      onDelete={() => this.handleTagDelete(tagName)}
      color="primary"
    />
  );

  handleCancel = () => {
    this.setState(initialState);
    this.props.close();
  };

  handlePanelChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false
    });
  };

  gotoNextPanel = (event, nextPanel) => {
    event.preventDefault();
    const product = { ...this.state };

    if (nextPanel === "panel2" && !product.unit) {
      this.setState({ unitHasError: true });
      return;
    }
    if (nextPanel === "panel2" && !product.category) {
      this.setState({ categoryHasError: true });
      return;
    }

    this.setState({
      expanded: nextPanel
    });
  };

  getPanelNumber = () => {
    const { expanded } = this.state;

    return parseInt(
      expanded.substring(expanded.length - 1, expanded.length),
      10
    );
  };

  render() {
    const { classes, open, close, units, inventories, categories } = this.props;

    const {
      unit,
      category,
      tag,
      tags,
      inventoryInfo,
      unitHasError,
      categoryHasError,
      expanded
    } = this.state;

    return (
      <Dialog
        open={open}
        onClose={() => close()}
        disableBackdropClick
        aria-labelledby="New Product"
        maxWidth="md"
      >
        <div className={styles.container}>
          <Paper square className={classes.paper}>
            <Typography variant="h5">New Product</Typography>

            <ExpansionPanel
              expanded={expanded === "panel1"}
              onChange={this.handlePanelChange("panel1")}
            >
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <Typography className={classes.subHeader}>
                  Product Details
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <form
                  id="panel1"
                  className={styles.formContainer}
                  autoComplete="off"
                  onSubmit={event => this.gotoNextPanel(event, "panel2")}
                >
                  <TextField
                    label="Product Name"
                    id="name"
                    className={classes.textField}
                    onBlur={this.handleChange}
                    margin="normal"
                    required
                  />
                  <TextField
                    label="SKU"
                    id="sku"
                    className={classes.textField}
                    onBlur={this.handleChange}
                    margin="normal"
                    type="number"
                  />

                  <FormControl className={classes.formControl}>
                    <Select
                      label="Select Unit"
                      id="unit"
                      value={unit}
                      onChange={this.handleChangeForSelect}
                      options={units}
                    />

                    {unitHasError && (
                      <FormHelperText>This is required!</FormHelperText>
                    )}
                  </FormControl>

                  <FormControl className={classes.formControl}>
                    <ReactSelect
                      options={categories.map(cat => ({
                        value: cat,
                        label: cat
                      }))}
                      value={category}
                      handleChange={this.handleChangeOfReactSelect}
                      placeholder="Select Category"
                    />
                    {categoryHasError && (
                      <FormHelperText>This is required!</FormHelperText>
                    )}
                  </FormControl>

                  <div>
                    <TextField
                      label="Add tag"
                      id="tag"
                      className={classes.textField}
                      onChange={this.handleChange}
                      margin="normal"
                    />

                    <Button
                      variant="contained"
                      color="secondary"
                      size="small"
                      className={classes.addButton}
                      onClick={this.addTag}
                      disabled={!tag}
                    >
                      Add
                    </Button>
                  </div>

                  {tags.map(t => this.getChip(t))}
                </form>
              </ExpansionPanelDetails>
            </ExpansionPanel>

            <ExpansionPanel
              expanded={expanded === "panel2"}
              onChange={this.handlePanelChange("panel2")}
              disabled={expanded && this.getPanelNumber() < 2}
            >
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <Typography className={classes.subHeader}>
                  Manufacturer Info
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <form
                  id="panel2"
                  className={styles.formContainer}
                  autoComplete="off"
                  onSubmit={event => this.gotoNextPanel(event, "panel3")}
                >
                  <TextField
                    label="Manufacturer"
                    id="manufacturerInfo-manufacturer"
                    className={classes.textField}
                    onBlur={this.handleChange}
                    margin="normal"
                    required
                  />

                  <TextField
                    label="Brand"
                    id="manufacturerInfo-brand"
                    className={classes.textField}
                    onBlur={this.handleChange}
                    margin="normal"
                  />

                  <TextField
                    label="UPC"
                    id="manufacturerInfo-upc"
                    className={classes.textField}
                    onBlur={this.handleChange}
                    margin="normal"
                    type="number"
                  />

                  <TextField
                    label="ISBN"
                    id="manufacturerInfo-isbn"
                    className={classes.textField}
                    onBlur={this.handleChange}
                    margin="normal"
                    type="number"
                  />
                </form>
              </ExpansionPanelDetails>
            </ExpansionPanel>

            <ExpansionPanel
              expanded={expanded === "panel3"}
              onChange={this.handlePanelChange("panel3")}
              disabled={expanded && this.getPanelNumber() < 3}
            >
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <Typography className={classes.subHeader}>
                  Sales Info
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <form
                  id="panel3"
                  className={styles.formContainer}
                  autoComplete="off"
                  onSubmit={event => this.gotoNextPanel(event, "panel4")}
                >
                  <TextField
                    label="Selling Price"
                    id="salesInfo-sellingPrice"
                    className={classes.textField}
                    onBlur={this.handleChange}
                    margin="normal"
                    required
                    type="number"
                  />
                  <TextField
                    label="Cost Price"
                    id="salesInfo-costPrice"
                    className={classes.textField}
                    onBlur={this.handleChange}
                    margin="normal"
                    required
                    type="number"
                  />
                  <TextField
                    label="Tax %"
                    id="salesInfo-tax"
                    className={classes.textField}
                    onBlur={this.handleChange}
                    margin="normal"
                    required
                    type="number"
                    max={100}
                  />
                </form>
              </ExpansionPanelDetails>
            </ExpansionPanel>

            <ExpansionPanel
              expanded={expanded === "panel4"}
              onChange={this.handlePanelChange("panel4")}
              disabled={expanded && this.getPanelNumber() !== 4}
            >
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <Typography className={classes.subHeader}>
                  Inventory Info
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <form
                  id="panel4"
                  className={styles.formContainer}
                  autoComplete="off"
                  onSubmit={this.save}
                >
                  <FormControl className={classes.formControl}>
                    <Select
                      label="Select Inventory"
                      id="inventoryInfo-inventoryId"
                      value={inventoryInfo.inventoryId}
                      onChange={this.handleChangeForSelect}
                      options={inventories.map(inv => ({
                        value: inv.id,
                        label: inv.name
                      }))}
                    />
                  </FormControl>

                  <TextField
                    label="Opening Stock"
                    id="inventoryInfo-availableStock"
                    className={classes.textField}
                    onBlur={this.handleChange}
                    margin="normal"
                    type="number"
                  />

                  <TextField
                    label="Restock Level"
                    id="inventoryInfo-reorderLevel"
                    className={classes.textField}
                    onBlur={this.handleChange}
                    margin="normal"
                    type="number"
                  />
                </form>
              </ExpansionPanelDetails>
            </ExpansionPanel>
            <div className={styles.buttonContainer}>
              <Button
                variant="contained"
                color={expanded === "panel4" ? "primary" : "secondary"}
                size="small"
                className={classes.button}
                type="submit"
                form={expanded || "panel4"}
                disabled={!expanded}
              >
                {expanded === "panel4" ? "Save" : "Next"}
              </Button>
              <Button
                variant="contained"
                color="primary"
                onClick={this.handleCancel}
                size="small"
                className={classes.button}
              >
                Cancel
              </Button>
            </div>
          </Paper>
        </div>
      </Dialog>
    );
  }
}

const mapStateToProps = state => ({
  product: state.product,
  units: state.config.data.units,
  inventories: state.config.data.inventories,
  categories: state.categories.data
});

const mapDispatchToProps = dispatch => ({
  saveProduct: (product, refetchProductsFlag) =>
    dispatch(saveProduct(product, refetchProductsFlag))
});

export default withStyles(materialStyles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Product)
);
