import axios from "axios";
import {
  SAVE_PRODUCT_LOADING,
  SAVE_PRODUCT_SUCCESS,
  SAVE_PRODUCT_ERROR,
  SEARCH_PRODUCTS_LOADING,
  SEARCH_PRODUCTS_SUCCESS,
  SEARCH_PRODUCTS_ERROR,
  CLEAR_PRODUCTS,
  FETCH_PRODUCT_LOADING,
  FETCH_PRODUCT_SUCCESS,
  FETCH_PRODUCT_ERROR,
  CLEAR_PRODUCT,
  FETCH_CATEGORIES_LOADING,
  FETCH_CATEGORIES_SUCCESS,
  FETCH_CATEGORIES_ERROR,
  UPDATE_INVENTORY_LOADING,
  UPDATE_INVENTORY_SUCCESS,
  UPDATE_INVENTORY_ERROR,
  DELETE_PRODUCT_LOADING,
  DELETE_PRODUCT_SUCCESS,
  DELETE_PRODUCT_ERROR
} from "../constants/actions";
import { openSnackbar } from "./snackbar";

export const saveProductLoading = () => ({
  type: SAVE_PRODUCT_LOADING
});

export const saveProductSuccess = data => ({
  type: SAVE_PRODUCT_SUCCESS,
  data
});

export const saveProductError = err => ({
  type: SAVE_PRODUCT_ERROR,
  err
});

export const searchProductsLoading = () => ({
  type: SEARCH_PRODUCTS_LOADING
});

export const searchProductsSuccess = data => ({
  type: SEARCH_PRODUCTS_SUCCESS,
  data
});

export const searchProductsError = err => ({
  type: SEARCH_PRODUCTS_ERROR,
  err
});

export const clearProducts = () => ({
  type: CLEAR_PRODUCTS
});

export const fetchProductLoading = () => ({
  type: FETCH_PRODUCT_LOADING
});

export const fetchProductSuccess = data => ({
  type: FETCH_PRODUCT_SUCCESS,
  data
});

export const fetchProductError = err => ({
  type: FETCH_PRODUCT_ERROR,
  err
});

export const clearProduct = () => ({
  type: CLEAR_PRODUCT
});

export const fetchCategoriesLoading = () => ({
  type: FETCH_CATEGORIES_LOADING
});

export const fetchCategoriesSuccess = data => ({
  type: FETCH_CATEGORIES_SUCCESS,
  data
});

export const fetchCategoriesError = err => ({
  type: FETCH_CATEGORIES_ERROR,
  err
});

export const updateInventoryLoading = () => ({
  type: UPDATE_INVENTORY_LOADING
});

export const updateInventorySuccess = data => ({
  type: UPDATE_INVENTORY_SUCCESS,
  data
});

export const updateInventoryError = err => ({
  type: UPDATE_INVENTORY_ERROR,
  err
});

export const deleteProductLoading = () => ({
  type: DELETE_PRODUCT_LOADING
});

export const deleteProductSuccess = data => ({
  type: DELETE_PRODUCT_SUCCESS,
  data
});

export const deleteProductError = err => ({
  type: DELETE_PRODUCT_ERROR,
  err
});

export const searchProducts = (
  query,
  page,
  size,
  sortDirection,
  sortBy,
  inventoryInfoOnly
) => (dispatch, getState) => {
  dispatch(searchProductsLoading());

  const state = getState();

  axios
    .get("https://inv-services.herokuapp.com/product", {
      params: {
        storeId: state.userDetails.data.storeId,
        q: query,
        page,
        size,
        sortDirection,
        sortBy,
        inventoryInfoOnly
      }
    })
    .then(resp => {
      dispatch(searchProductsSuccess(resp.data));
    })
    .catch(err => dispatch(searchProductsError(err)));
};

export const saveProduct = (product, refetchProductsFlag) => (
  dispatch,
  getState
) => {
  dispatch(saveProductLoading());

  const state = getState();

  product.storeId = state.userDetails.data.storeId;

  axios({
    method: "post",
    url: "https://inv-services.herokuapp.com/product",
    data: product
  })
    .then(resp => {
      dispatch(saveProductSuccess(resp.data));

      if (refetchProductsFlag) {
        console.log("test");
        dispatch(searchProducts("", 0, 10, "", "", "Y"));
      }

      dispatch(
        openSnackbar({
          variant: "success",
          open: true,
          message: "Product saved."
        })
      );
    })
    .catch(err => dispatch(saveProductError(err)));
};

export const fetchProductById = id => dispatch => {
  dispatch(fetchProductLoading());

  axios
    .get(`https://inv-services.herokuapp.com/product/${id}`)
    .then(resp => {
      dispatch(fetchProductSuccess(resp.data));
    })
    .catch(err => dispatch(fetchProductError(err)));
};

export const fetchCategories = () => (dispatch, getState) => {
  dispatch(fetchCategoriesLoading());

  const state = getState();

  axios
    .get("https://inv-services.herokuapp.com/product/categories", {
      params: {
        storeId: state.userDetails.data.storeId
      }
    })
    .then(resp => {
      dispatch(fetchCategoriesSuccess(resp.data));
    })
    .catch(err => dispatch(fetchCategoriesError(err)));
};

export const updateInventory = (
  products,
  refetchProductsFlag,
  page
) => dispatch => {
  dispatch(updateInventoryLoading());

  axios({
    method: "put",
    url: "https://inv-services.herokuapp.com/product/inventory",
    data: products
  })
    .then(resp => {
      dispatch(updateInventorySuccess(resp.data));

      if (refetchProductsFlag) {
        dispatch(fetchProductById(products[0].id));
        dispatch(searchProducts("", page, 10, "", "", "Y"));
      }
    })
    .catch(err => dispatch(updateInventoryError(err)));
};

export const deleteProduct = (id, refetchProductsFlag, page) => dispatch => {
  dispatch(deleteProductLoading());

  axios({
    method: "delete",
    url: `https://inv-services.herokuapp.com/product/${id}`
  })
    .then(() => {
      dispatch(deleteProductSuccess());

      dispatch(clearProduct());
      if (refetchProductsFlag) {
        dispatch(searchProducts("", page, 10, "", "", "Y"));
      }

      dispatch(
        openSnackbar({
          variant: "success",
          open: true,
          message: "Product deleted."
        })
      );
    })
    .catch(err => dispatch(deleteProductError(err)));
};
