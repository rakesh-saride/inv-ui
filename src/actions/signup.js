import axios from "axios";
import {
  SIGNUP_LOADING,
  SIGNUP_SUCCESS,
  SIGNUP_ERROR
} from "../constants/actions";

export const signupLoading = () => ({
  type: SIGNUP_LOADING
});

export const signupSuccess = data => ({
  type: SIGNUP_SUCCESS,
  data
});

export const signupError = err => ({
  type: SIGNUP_ERROR,
  err
});

export const signup = credentials => dispatch => {
  dispatch(signupLoading());

  axios({
    method: "post",
    url: "https://inv-services.herokuapp.com/user/signup",
    data: credentials
  })
    .then(resp => {
      dispatch(signupSuccess(resp.data));
    })
    .catch(err => dispatch(signupError(err)));
};
