import axios from "axios";
import {
  LOGIN_LOADING,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGOUT
} from "../constants/actions";
import { openSnackbar } from "./snackbar";
import { persistor } from "../store";

export const loginLoading = () => ({
  type: LOGIN_LOADING
});

export const loginSuccess = (authorizationToken, data) => ({
  type: LOGIN_SUCCESS,
  data: {
    ...data,
    authorizationToken
  }
});

export const loginError = err => ({
  type: LOGIN_ERROR,
  err
});

export const logout = () => dispatch => {
  persistor.purge();
  dispatch({
    type: LOGOUT
  });
};

export const login = credentials => dispatch => {
  dispatch(loginLoading());

  axios({
    method: "post",
    url: "https://inv-services.herokuapp.com/login",
    data: credentials
  })
    .then(resp => {
      axios
        .get("https://inv-services.herokuapp.com/user/details", {
          headers: { Authorization: resp.headers.authorization }
        })
        .then(response => {
          dispatch(loginSuccess(resp.headers.authorization, response.data));
        });
    })
    .catch(err => {
      if (err.response && err.response.status === 403) {
        dispatch(
          openSnackbar({
            variant: "error",
            open: true,
            message: "Invalid credentials."
          })
        );
      }
      dispatch(loginError(err));
    });
};
