import axios from "axios";
import {
  FETCH_UNITS_LOADING,
  FETCH_UNITS_SUCCESS,
  FETCH_UNITS_ERROR,
  FETCH_DISCOUNTS_LOADING,
  FETCH_DISCOUNTS_SUCCESS,
  FETCH_DISCOUNTS_ERROR,
  FETCH_INVENTORY_LOADING,
  FETCH_INVENTORY_SUCCESS,
  FETCH_INVENTORY_ERROR
} from "../constants/actions";

export const fetchUnitsLoading = () => ({
  type: FETCH_UNITS_LOADING
});

export const fetchUnitsSuccess = data => ({
  type: FETCH_UNITS_SUCCESS,
  data
});

export const fetchUnitsError = err => ({
  type: FETCH_UNITS_ERROR,
  err
});

export const fetchUnits = () => dispatch => {
  dispatch(fetchUnitsLoading());

  axios
    .get("https://inv-services.herokuapp.com/product/units")
    .then(resp => {
      dispatch(fetchUnitsSuccess(resp.data));
    })
    .catch(err => dispatch(fetchUnitsError(err)));
};

export const fetchDiscountsLoading = () => ({
  type: FETCH_DISCOUNTS_LOADING
});

export const fetchDiscountsSuccess = data => ({
  type: FETCH_DISCOUNTS_SUCCESS,
  data
});

export const fetchDiscountsError = err => ({
  type: FETCH_DISCOUNTS_ERROR,
  err
});

export const fetchInventoryLoading = () => ({
  type: FETCH_INVENTORY_LOADING
});

export const fetchInventorySuccess = data => ({
  type: FETCH_INVENTORY_SUCCESS,
  data
});

export const fetchInventoryError = err => ({
  type: FETCH_INVENTORY_ERROR,
  err
});

export const fetchDiscounts = () => (dispatch, getState) => {
  dispatch(fetchDiscountsLoading());

  const state = getState();

  axios
    .get(
      `https://inv-services.herokuapp.com/discount/store/${
        state.userDetails.data.storeId
      }`
    )
    .then(resp => {
      dispatch(fetchDiscountsSuccess(resp.data));
    })
    .catch(err => dispatch(fetchDiscountsError(err)));
};

export const fetchInventoryDetails = () => (dispatch, getState) => {
  dispatch(fetchInventoryLoading());

  const state = getState();

  axios
    .get(
      `https://inv-services.herokuapp.com/inventory/store/${
        state.userDetails.data.storeId
      }`
    )
    .then(resp => {
      dispatch(fetchInventorySuccess(resp.data));
    })
    .catch(err => dispatch(fetchInventoryError(err)));
};
