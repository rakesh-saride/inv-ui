import { OPEN_SNACKBAR, CLOSE_SNACKBAR } from "../constants/actions";

export const openSnackbar = data => ({
  type: OPEN_SNACKBAR,
  data
});

export const closeSnackbar = () => ({
  type: CLOSE_SNACKBAR
});
