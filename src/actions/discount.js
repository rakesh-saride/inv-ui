import axios from "axios";
import {
  SAVE_DISCOUNT_LOADING,
  SAVE_DISCOUNT_SUCCESS,
  SAVE_DISCOUNT_ERROR
} from "../constants/actions";
import { fetchDiscounts } from "./config";

export const saveDiscountLoading = () => ({
  type: SAVE_DISCOUNT_LOADING
});

export const saveDiscountSuccess = data => ({
  type: SAVE_DISCOUNT_SUCCESS,
  data
});

export const saveDiscountError = err => ({
  type: SAVE_DISCOUNT_ERROR,
  err
});

export const saveDiscount = discount => (dispatch, getState) => {
  dispatch(saveDiscountLoading());

  const state = getState();
  discount.storeId = state.userDetails.data.storeId;

  axios({
    method: "post",
    url: "https://inv-services.herokuapp.com/discount",
    data: discount
  })
    .then(resp => {
      dispatch(saveDiscountSuccess(resp.data));
      dispatch(fetchDiscounts());
    })
    .catch(err => dispatch(saveDiscountError(err)));
};

export const deleteDiscount = id => dispatch => {
  dispatch(saveDiscountLoading());

  axios({
    method: "delete",
    url: `https://inv-services.herokuapp.com/discount/${id}`
  })
    .then(() => {
      dispatch(fetchDiscounts());
    })
    .catch(err => dispatch(saveDiscountError(err)));
};
