const path = require("path");

const HtmlWebpackPlugin = require("html-webpack-plugin");
const BrowserSyncPlugin = require("browser-sync-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const UglifyWebpackPlugin = require("uglifyjs-webpack-plugin");

const BrowserSync = new BrowserSyncPlugin({
  proxy: "localhost:4200",
  port: 4201,
  notify: false,
  browser: "google chrome",
  host: "localhost",
  open: "external"
});

module.exports = env => {
  console.log("NODE_ENV: ", env);

  const devMode = env && env.NODE_ENV !== "production";

  return {
    entry: "./src/index.js",
    output: {
      filename: "[name].js",
      path: path.join(__dirname, "public"),
      publicPath: "/"
    },
    devServer: {
      historyApiFallback: true,
      // allowedHosts: ["abc.com"],
      port: 4200
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loaders: ["babel-loader"]
        },
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: "css-loader",
              options: {
                modules: true,
                localIdentName: devMode
                  ? "[local]_[hash:base64:5]"
                  : "[hash:base64:5]"
              }
            }
          ]
        },
        {
          test: /\.(png|svg|jpg|gif|ico)$/,
          use: ["file-loader?name=[name].[ext]"]
        }
      ]
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: "src/index.html"
      }),
      new MiniCssExtractPlugin({
        filename: devMode ? "[name].css" : "[name].[hash].css",
        chunkFilename: devMode ? "[id].css" : "[id].[hash].css"
      }),
      BrowserSync
    ],
    optimization: {
      minimizer: [
        new UglifyWebpackPlugin({
          test: /\.js$/,
          exclude: /node_modules/
        })
      ]
    }
  };
};
